﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoginSceneController : BaseController
{
    public TMP_InputField usernameTextInput;

    // Use this for initialization
    void Start()
    {
        Base.Instance.Init();
    }

    public void OnButtonClicked(string btnName)
    {
        switch (btnName)
        {
            case "login":
                if (playerData.account == null)
                    playerData.account = new Account();
                if (!string.IsNullOrEmpty(usernameTextInput.text))
                {
                    playerData.account.name = usernameTextInput.text;
                }
                else
                {
                    playerData.account.name = "Guest" + Random.Range(1000000, 9999999);
                }
                SaveManager.Instance.Save();
                PhotonNetwork.player.NickName = playerData.account.name;
                changeScene(SceneController.SceneID.LevelScene);
                //changeScene(SceneController.SceneID.GameScreen);
                break;
            case "register":
                changeScene(SceneController.SceneID.GameScreen);
                break;
            case "setting":
                WindowManager.ShowWindow("WSetting");
                break;
        }
    }

    public void RoomBtnClicked(string btnName)
    {
        Base.Instance.networkManager.roomSelected = btnName;
        if (playerData.account == null)
            playerData.account = new Account();
        if (!string.IsNullOrEmpty(usernameTextInput.text)) {
            playerData.account.name = usernameTextInput.text;
        } else {
            playerData.account.name = "Guest" + Random.Range(1000000, 9999999);
        }
        SaveManager.Instance.Save();
        PhotonNetwork.player.NickName = playerData.account.name;
        changeScene(SceneController.SceneID.LevelScene);
    }

}
