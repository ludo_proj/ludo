﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System;

public enum ViewState
{
    Default,
    JoiningRoom,
    InRoom
}

public class LevelSceneController : BaseController
{

    public TextMeshProUGUI playerName;

    public Button playButton;
    public Button inviteButton;
    public Button cancelButton;

    public PlayerProfile[] playerProfile;

    private Coroutine updatePlayerCor;

    private bool cancelJoinRoomCall;

    private ViewState _currentState;
    public ViewState CurrentState
    {
        get { return _currentState; }
        set
        {
            if (_currentState == value) return;
            if (value == ViewState.JoiningRoom || value == ViewState.InRoom)
            {
                if (_currentState == ViewState.Default)
                {
                    inviteButton.transform.DOLocalMoveX(inviteButton.transform.localPosition.x + inviteButton.gameObject.GetRectTransform().rect.width, .5f);
                    cancelButton.transform.DOLocalMoveX(cancelButton.transform.localPosition.x - cancelButton.gameObject.GetRectTransform().rect.width, .5f);
                }
                if (value == ViewState.InRoom) {
                    if (PhotonNetwork.player.IsMasterClient) {
                        playButton.transform.DOLocalMoveX(playButton.transform.localPosition.x - playButton.gameObject.GetRectTransform().rect.width, .5f);
                        playButton.GetComponentInChildren<TextMeshProUGUI>().text = "Play";
                    }
                    cancelButton.GetComponentInChildren<TextMeshProUGUI>().text = "Exit Room";
                }
                else {
                    playButton.transform.DOLocalMoveX(playButton.transform.localPosition.x + playButton.gameObject.GetRectTransform().rect.width, .5f);
                    cancelButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
                }
            }
            else
            {
                playButton.GetComponentInChildren<TextMeshProUGUI>().text = "Join";
                if (_currentState == ViewState.JoiningRoom || (_currentState == ViewState.InRoom && !PhotonNetwork.player.IsMasterClient))
                    playButton.transform.DOLocalMoveX(playButton.transform.localPosition.x - playButton.gameObject.GetRectTransform().rect.width, .5f);
                inviteButton.transform.DOLocalMoveX(inviteButton.transform.localPosition.x - inviteButton.gameObject.GetRectTransform().rect.width, .5f);
                cancelButton.transform.DOLocalMoveX(cancelButton.transform.localPosition.x + cancelButton.gameObject.GetRectTransform().rect.width, .5f);
            }
            _currentState = value;
        }
    }

    // Use this for initialization
    void Awake()
    {
        cancelJoinRoomCall = false;

        Base.Instance.Init();
    }

    void Start()
    {
        playerName.text = playerData.account.name;
    }

    void OnEnable()
    {
        Base.Instance.networkManager.OnRoomJoined += JoinRoomSuccess;
        Base.Instance.networkManager.OnFailedToJoinRoom += JoinRoomFailed;
        PhotonNetwork.OnEventCall += onEventCall;
    }

    void OnDisable()
    {
        if (Base.isQuitting) return;
        Base.Instance.networkManager.OnRoomJoined -= JoinRoomSuccess;
        Base.Instance.networkManager.OnFailedToJoinRoom -= JoinRoomFailed;
        PhotonNetwork.OnEventCall -= onEventCall;
    }

    public void OnButtonClicked(string btnName)
    {
        switch (btnName)
        {
            case "play":
                if (CurrentState == ViewState.Default) {
                    bool joinResult = Base.Instance.networkManager.JoinCertainRoom();
                    if (joinResult) CurrentState = ViewState.JoiningRoom;
                } else {
                    if (PhotonNetwork.playerList.Length >= 2) {
                        StopCoroutine(updatePlayerCor);
                        StartGame();
                    }
                }
                break;
            case "create":
                CurrentState = ViewState.JoiningRoom;
                Base.Instance.networkManager.createRoom();
                break;
            case "cancel":
                if (CurrentState == ViewState.InRoom)
                {
                    PhotonNetwork.room.IsOpen = true;
                    PhotonNetwork.room.IsVisible = true;
                    Base.Instance.networkManager.LeaveRoom();
                    OnRoomLeft();
                    CurrentState = ViewState.Default;
                }
                else if (CurrentState == ViewState.JoiningRoom)
                {
                    cancelJoinRoomCall = true;
                }
                break;
        }
    }

    private void JoinRoomFailed()
    {
        cancelJoinRoomCall = false;
        OnRoomLeft();
        CurrentState = ViewState.Default;
    }

    private void JoinRoomSuccess()
    {
        if (cancelJoinRoomCall)
        {
            cancelJoinRoomCall = false;
            PhotonNetwork.room.IsOpen = true;
            PhotonNetwork.room.IsVisible = true;
            Base.Instance.networkManager.LeaveRoom();
            OnRoomLeft();
            CurrentState = ViewState.Default;
            return;
        }

        CurrentState = ViewState.InRoom;
        updatePlayerCor = StartCoroutine(UpdatePlayerList());
    }

    IEnumerator UpdatePlayerList()
    {
        yield return 0;

        while (true)
        {
            for (int i = 0; i < playerProfile.Length; i++)
            {
                if (i >= PhotonNetwork.playerList.Length)
                {
                    playerProfile[i].ClearPlayerData();
                }
            }

            Array.Sort(PhotonNetwork.playerList, (PhotonPlayer x, PhotonPlayer y) =>
            {
                if (x.ID < y.ID)
                    return -1;
                else
                    return 1;
            });

            int countPpl = 0;
            for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
            {
                countPpl++;
                playerProfile[i].PlayerJoined(PhotonNetwork.playerList[i].NickName, PhotonNetwork.playerList[i].ID);
                if (playerProfile[i].userId == PhotonNetwork.player.ID) {
                    Base.Instance.networkManager.PlayerIdx = i + 1;
                }
            }

            yield return null;

            //if (countPpl >= 4)
            //{
            //    // if (PhotonNetwork.isMasterClient)
            //    // {
            //    yield return new WaitForSeconds(1f);
            //    //     PhotonNetwork.RaiseEvent((int)EnumPhoton.StartGame, null, true, null);
            //    StartGame();
            //    // }
            //    break;
            //}
        }
    }

    void onEventCall(byte eventcode, object content, int senderid)
    {
        Base.Instance.textController.updateText("eventCode: " + ((EnumPhoton)eventcode).ToString() + " content: " + content + " senderid: " + senderid);
        if (eventcode == (int)EnumPhoton.JoinedRoom)
        {
            if (PhotonNetwork.room.PlayerCount >= 4)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    //PhotonNetwork.room.IsOpen = false;
                    PhotonNetwork.room.IsVisible = false;
                }
            }
        }
        else if (eventcode == (int)EnumPhoton.StartGame)
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        Base.Instance.networkManager.setPlayers(playerProfile);
        changeScene(SceneController.SceneID.GameScreen);
    }

    public void OnRoomLeft()
    {
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            playerProfile[i].ClearPlayerData();
        }
        if (updatePlayerCor != null) StopCoroutine(updatePlayerCor);
    }

}
