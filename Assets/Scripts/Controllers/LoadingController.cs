﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadingController : MonoBehaviour {
    public Image progressBar;
    public TextMeshProUGUI progressText;

	// Use this for initialization
	void Start () {
        progressBar.fillAmount = 0;
        progressText.text = "0 %";
	}
	
	// Update is called once per frame
	public void UpdateProgress(float progress) {
        progressBar.fillAmount = progress;
        progressText.text = (progress * 100) + " %";
	}
}
