using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugTextController : MonoBehaviour
{
    public TextMeshProUGUI text;

    public void updateText(object message)
    {
        Debug.Log(message.ToString());
        text.text += message.ToString() + "\n";
    }

    public void OnButtonClicked(string btnName) {
        switch (btnName) {
            case "toggle":
                text.gameObject.SetActive(!text.gameObject.activeSelf);
                break;
        }
    }
}