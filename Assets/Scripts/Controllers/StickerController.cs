﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class StickerController : MonoBehaviour {
    //Action button's constant
    public const string LEAVE_GAME = "leave_game";

    public StickerManager.StickerID stickerID;
    public Image charImage;
    public Image targetCharImage;
    public Image flagImage;
    public TextMeshProUGUI textDesc;
    public Button actionButton;
    public bool autoHide;

    Animation anim;

	void Awake () {
        anim = GetComponent<Animation>();
	}

    public void Initialize(StickerManager.StickerData data) {
        if (stickerID == StickerManager.StickerID.PlayerKick) {
            charImage.sprite = Base.Instance.stickerManager.stickerSprites["kicker_" + data.sourceIdx];
            targetCharImage.sprite = Base.Instance.stickerManager.stickerSprites["kicked_" + data.targetIdx];
        } else if (stickerID == StickerManager.StickerID.PlayerWin) {
            charImage.sprite = Base.Instance.stickerManager.stickerSprites["winner_" + data.sourceIdx];
            flagImage.sprite = Base.Instance.stickerManager.stickerSprites["winflag_" + data.winRank];
        } else if (stickerID == StickerManager.StickerID.PlayerTurn) {
            charImage.sprite = Base.Instance.stickerManager.stickerSprites["turn_" + data.sourceIdx];
            flagImage.sprite = Base.Instance.stickerManager.stickerSprites["turn_bg_" + data.sourceIdx];
            switch (data.sourceIdx) {
                case 1: textDesc.color = Color.green; textDesc.text = "GREEN'S\nTURN"; break;
                case 2: textDesc.color = Color.red; textDesc.text = "RED'S\nTURN"; break;
                case 3: textDesc.color = Color.blue; textDesc.text = "BLUE'S\nTURN"; break;
                case 4: textDesc.color = Color.yellow; textDesc.text = "YELLOW'S\nTURN"; break;
            }
        } else if (stickerID == StickerManager.StickerID.PlayerLose) {
            charImage.sprite = Base.Instance.stickerManager.stickerSprites["loser_" + data.sourceIdx];
        }
    }
	
	public void Play(Action completeAction) {
        StartCoroutine(PlayAnim(completeAction));
    }

    IEnumerator PlayAnim(Action completeAction) {
        anim.Play();

        while (anim.isPlaying)
            yield return null;

        yield return new WaitForSeconds(.3f);

        if (completeAction != null)
            completeAction.Invoke();

        if (!autoHide) {
            actionButton.transform.DOScale(1f, .8f);
        }
    }

    public void OnButtonClicked(string btnName) {
        switch (btnName) {
            case LEAVE_GAME:
                Base.Instance.stickerManager.HideSticker();
                Base.Instance.networkManager.LeaveRoom();
                SceneController.Instance.loadScene(SceneController.SceneID.LevelScene);
                break;
            default:
                break;
        }
    }
}
