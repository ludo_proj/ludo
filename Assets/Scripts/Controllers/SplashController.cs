using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashController : BaseController
{
    void Awake()
    {
        Base.Instance.Init();
    }

    void Start()
    {
        changeScene(SceneController.SceneID.LoginScene);
    }
}