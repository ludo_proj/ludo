using System;

[Serializable]
public class Account
{
    public string name;
    public int credit;

    public Account()
    {
        name = "";
        credit = 0;
    }

    public Account(string name, int credit)
    {
        this.name = name;
        this.credit = credit;
    }
}