using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager
{
    public static string SavePath
    {
        get
        {
            return Application.persistentDataPath + "/savedGames.gd";
        }
    }

    public static SaveManager Instance
    {
        get
        {
            if (Base.Instance.saveManager == null)
            {
                Base.Instance.saveManager = new SaveManager();
            }
            return Base.Instance.saveManager;
        }
    }

    public PlayerData playerData;

    public SaveManager()
    {
        Init();
    }

    void Init()
    {
        string loadDataStr = TryLoadGame();
        if (!string.IsNullOrEmpty(loadDataStr)) {
            Debug.Log(loadDataStr);
            try {
                playerData = JsonConvert.DeserializeObject<PlayerData>(loadDataStr);
            } catch (JsonReaderException ex) {
                Debug.Log("Read data error; " + ex.Message);
            }
        } else {
            playerData = new PlayerData();
        }
    }

    private string TryLoadGame()
    {
        string result = "";
        if (File.Exists(SavePath))
        {
            result = File.ReadAllText(SavePath);
        }

        return result;
    }

    public void Save()
    {
        string data = JsonConvert.SerializeObject(playerData);
        File.WriteAllBytes(SavePath, System.Text.Encoding.UTF8.GetBytes(data));
    }
}