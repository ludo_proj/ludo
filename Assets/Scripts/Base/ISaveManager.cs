public interface ISaveManager
{
    void setState(object variableName, object value);
    void setVolatileState(object variableName, object value);
    void saveData();
}