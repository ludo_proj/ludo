using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : Singleton<Base>
{

    public SaveManager saveManager;

    public GameObject upperUICanvas;
    public Transform windowLayer;
    public LoadingController loadingLayer;
    public NetworkManager networkManager;
    public StickerManager stickerManager;
    public DebugTextController textController;
    public GameObject cloudContainer;
    public Image backgroundImg;

    private FPSController fpsController;

    public Dictionary<string, Sprite> allSprites;

    public bool alreadyInit = false;

    protected Base() { } // guarantee this will be always a singleton only - can't use the constructor!

    public static bool isQuitting = false;

    public Action OnResolutionChanged;

    public void Init()
    {
        if (alreadyInit) return;
        alreadyInit = true;

        saveManager = SaveManager.Instance;

        allSprites = new Dictionary<string, Sprite>();

        Sprite[] sprites = Resources.LoadAll<Sprite>("AllScene");
        foreach (Sprite spr in sprites) {
            allSprites.Add(spr.name, spr);
        }

        stickerManager = new GameObject("StickerManager").AddComponent<StickerManager>();
        DontDestroyOnLoad(stickerManager);

        upperUICanvas = Instantiate(Resources.Load<GameObject>("Prefabs/Upper UI Canvas"));
        backgroundImg = upperUICanvas.transform.Find("Background").gameObject.GetComponent<Image>();
        fpsController = upperUICanvas.AddComponent<FPSController>();
        DontDestroyOnLoad(upperUICanvas);

        cloudContainer = Instantiate(Resources.Load<GameObject>("Prefabs/CloudContainer"), upperUICanvas.transform);

        windowLayer = Instantiate(Resources.Load<GameObject>("Prefabs/WindowLayer"), upperUICanvas.transform).transform;
        windowLayer.name = "windowLayer";

        loadingLayer = Instantiate(Resources.Load<GameObject>("Prefabs/LoadingLayer"), upperUICanvas.transform).GetComponent<LoadingController>();
        loadingLayer.transform.name = "loadingLayer";
        loadingLayer.gameObject.SetActive(false);

        networkManager = Instantiate(Resources.Load<GameObject>("Prefabs/NetworkManager")).GetComponent<NetworkManager>();
        networkManager.name = "networkManager";
        DontDestroyOnLoad(networkManager.gameObject);
        
        textController = Instantiate(Resources.Load<GameObject>("Prefabs/DebugText"), upperUICanvas.transform).GetComponent<DebugTextController>();
        textController.name = "textController";

        Application.logMessageReceived += handleUnityLog;
    }

    private void handleUnityLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error)
        {
            textController.updateText(stackTrace);
        }
    }

    public void ResetCamera()
    {
        windowLayer.GetComponent<Canvas>().worldCamera = Camera.main;
    }

    void OnApplicationQuit()
    {
        Debug.Log("IsQuitting");
        isQuitting = true;
        PhotonNetwork.Disconnect();
    }

}