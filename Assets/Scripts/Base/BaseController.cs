using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BaseController : MonoBehaviour, ISaveManager
{

    public PlayerData playerData
    {
        get
        {
            return SaveManager.Instance.playerData;
        }
    }

    public void setState(object variable, object value)
    {
        setVolatileState(variable, value);
        saveData();
    }

    public void setVolatileState(object variable, object value)
    {
        variable = value;
    }

    public void saveData()
    {
        SaveManager.Instance.Save();
    }
    
    public void changeScene(SceneController.SceneID sceneID)
    {
        SceneController.Instance.loadScene(sceneID);
    }
}