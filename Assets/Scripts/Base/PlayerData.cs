using System;

[Serializable]
public class PlayerData
{
    public Account account;
    public string token;
    public bool muteMusic;
    public bool muteSound;
}