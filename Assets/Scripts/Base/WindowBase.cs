﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Base
{
    public class WindowBase : MonoBehaviour
    {
        [HideInInspector]
        public bool blockClick;
        public bool closeOnBackPressed;

        public void OnShowed()
        {

        }
        public virtual void InitWithData(object data)
        {

        }
        public void OnClosed()
        {
            Destroy(gameObject);
        }

        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape) && closeOnBackPressed)
            {
                Close();
            }
        }

        public void Close()
        {
            WindowManager.CloseCurrentActiveWindow();
        }
    }
}
