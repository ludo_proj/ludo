using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController
{
    public enum SceneID
    {
        LoginScene,
        LevelScene,
        GameScreen
    }

    private static SceneController _instance;

    public static SceneController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new SceneController();
            }
            return _instance;
        }
    }

    public void loadScene(SceneID sceneID)
    {
        Base.Instance.StartCoroutine(loadSceneAsyncCour(sceneID));
    }

    IEnumerator loadSceneAsyncCour(SceneID sceneID)
    {
        Base.Instance.loadingLayer.UpdateProgress(0);
        Base.Instance.loadingLayer.gameObject.SetActive(true);
        AsyncOperation syncLoad = SceneManager.LoadSceneAsync((int)sceneID, LoadSceneMode.Single);
        while (!syncLoad.isDone)
        {
            yield return null;

            Base.Instance.loadingLayer.UpdateProgress(syncLoad.progress);
        }
        yield return null;
        Base.Instance.cloudContainer.SetActive(sceneID != SceneID.GameScreen);
        Base.Instance.backgroundImg.gameObject.SetActive(sceneID != SceneID.GameScreen);
        Base.Instance.loadingLayer.UpdateProgress(1);
        Base.Instance.loadingLayer.gameObject.SetActive(false);
        Base.Instance.ResetCamera();
    }
}