﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStack : MonoBehaviour {
    List<Material> materialList;
    MeshRenderer[] allMeshes;

    void Awake () {
        materialList = new List<Material>();
        allMeshes = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < allMeshes.Length; i++) {
            materialList.Add(allMeshes[i].material);
        }
	}

    public void Initialize(List<PawnController> pawnControllers) {
        for (int i = 0; i < pawnControllers.Count - 1; i++) {
            materialList[i] = pawnControllers[i].meshRenderer.materials[0];
            allMeshes[i].material = materialList[i];
        }
    }

}
