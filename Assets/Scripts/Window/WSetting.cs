﻿using Assets.Scripts.Base;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WSetting : WindowBase {
    public Button musicBtn;
    public Button soundBtn;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    public void OnButtonClicked(string btnName) {
        switch (btnName) {
            case "music":
                SaveManager.Instance.playerData.muteMusic = !SaveManager.Instance.playerData.muteMusic;
                UpdateMusicBtn();
                break;
            case "sound":
                SaveManager.Instance.playerData.muteSound = !SaveManager.Instance.playerData.muteSound;
                UpdateSoundBtn();
                break;
            case "leave":
                Close();
                Base.Instance.networkManager.LeaveRoom();
                SceneController.Instance.loadScene(SceneController.SceneID.LevelScene);
                break;
        }
    }

    private void UpdateSoundBtn() {
        SpriteState spriteState = new SpriteState();
        if (SaveManager.Instance.playerData.muteSound) {
            soundBtn.GetComponentInChildren<TextMeshProUGUI>().text = "Off";
            soundBtn.image.sprite = Base.Instance.allSprites["red_button"];
            spriteState.pressedSprite = Base.Instance.allSprites["red_button_clicked"];
        } else {
            soundBtn.GetComponentInChildren<TextMeshProUGUI>().text = "On";
            soundBtn.image.sprite = Base.Instance.allSprites["green_button"];
            spriteState.pressedSprite = Base.Instance.allSprites["green_button_clicked"];
        }
        soundBtn.spriteState = spriteState;
    }

    private void UpdateMusicBtn() {
        SpriteState spriteState = new SpriteState();
        if (SaveManager.Instance.playerData.muteMusic) {
            musicBtn.GetComponentInChildren<TextMeshProUGUI>().text = "Off";
            musicBtn.image.sprite = Base.Instance.allSprites["red_button"];
            spriteState.pressedSprite = Base.Instance.allSprites["red_button_clicked"];
        } else {
            musicBtn.GetComponentInChildren<TextMeshProUGUI>().text = "On";
            musicBtn.image.sprite = Base.Instance.allSprites["green_button"];
            spriteState.pressedSprite = Base.Instance.allSprites["green_button_clicked"];
        }
        musicBtn.spriteState = spriteState;
    }
}
