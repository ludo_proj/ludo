﻿using Assets.Scripts.Base;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WInfo : WindowBase {
    public struct InfoData {
        public string title;
        public string desc;
    }

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descText;

    InfoData infoData;

	// Use this for initialization
	public override void InitWithData (object data) {
        infoData = (InfoData)data;
		if (!string.IsNullOrEmpty(infoData.title)) {
            titleText.text = infoData.title;
        }
        if (!string.IsNullOrEmpty(infoData.desc)) {
            descText.text = infoData.desc;
        }
    }
	
	// Update is called once per frame
	public void OnButtonClicked (string btnName) {
        switch (btnName) {
            case "ok":
                Close();
                break;
        }
	}
}
