﻿using Assets.Scripts.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WGameWin : WindowBase {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    public void OnButtonClicked(string btnName) {
        switch (btnName) {
            case "leave":
                Close();
                Base.Instance.networkManager.LeaveRoom();
                SceneController.Instance.loadScene(SceneController.SceneID.LevelScene);
                break;
        }
    }
}
