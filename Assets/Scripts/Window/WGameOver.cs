﻿using Assets.Scripts.Base;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WGameOver : WindowBase {
    public struct GameOverData {
        public string title;
        public string desc;
    }

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descText;

    GameOverData infoData;

    // Use this for initialization
    public override void InitWithData(object data) {
        infoData = (GameOverData)data;
        if (!string.IsNullOrEmpty(infoData.title)) {
            titleText.text = infoData.title;
        }
        if (!string.IsNullOrEmpty(infoData.desc)) {
            descText.text = infoData.desc;
        }
    }

    // Update is called once per frame
    public void OnButtonClicked(string btnName) {
		switch (btnName) {
            case "leave":
                Close();
                Base.Instance.networkManager.LeaveRoom();
                SceneController.Instance.loadScene(SceneController.SceneID.LevelScene);
                break;
        }
	}
}
