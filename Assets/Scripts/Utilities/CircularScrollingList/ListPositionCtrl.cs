﻿using System;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;

public class ListPositionCtrl : MonoBehaviour
{
	public enum Direction
	{
		VERTICAL,
		HORIZONTAL
	};

	public static ListPositionCtrl Instance;
    
	public bool controlByButton = false;
	public bool alignToCenter = false;

	public ListBox[] listBoxes;
	public Button[] buttons;

	public Direction direction = Direction.VERTICAL;
	public float divideFactor = 2.0f;
	public int slidingFrames = 35;
	[Range(0.0f, 1.0f)]
	public float slidingFactor = 0.2f;
	[Range(-1.0f, 1.0f)]
	public float angularity = 0.3f;
	[Range(-1.0f, 1.0f)]
	public float positionAdjust = -0.7f;
	public float scaleFactor = 0.32f;
    
	private Canvas _parentCanvas;
    private MetaGesture metaGesture;

	private Vector2 _canvasMaxPos_L;
	private Vector2 _unitPos_L;
	private Vector2 _lowerBoundPos_L;
	private Vector2 _upperBoundPos_L;
	private Vector2 _shiftBoundPos_L;
	public Vector2 canvasMaxPos_L { get { return _canvasMaxPos_L; } }
	public Vector2 unitPos_L { get { return _unitPos_L; } }
	public Vector2 lowerBoundPos_L { get { return _lowerBoundPos_L; } }
	public Vector2 upperBoundPos_L { get { return _upperBoundPos_L; } }
	public Vector2 shiftBoundPos_L { get { return _shiftBoundPos_L; } }
    
	private Vector3 _startInputPos_L;
	private Vector3 _lastInputPos_L;
	private Vector3 _currentInputPos_L;
	private Vector3 _deltaInputPos_L;
	private int _numofSlideFrames;
    
	private Vector3 _alignToCenterDistance;

    [HideInInspector]
    public int currentSelectedContent;
    public string CurrentSelectedRoom
    {
        get {
            return ListBank.Instance.getListContent(currentSelectedContent);
        }
    }

    void Awake()
	{
		Instance = this;
        metaGesture = GetComponent<MetaGesture>();
	}
    
	void Start()
	{
		_parentCanvas = GetComponentInParent<Canvas>();
        
		RectTransform rectTransform = _parentCanvas.GetComponent<RectTransform>();
		_canvasMaxPos_L = new Vector2(rectTransform.rect.width / 2, rectTransform.rect.height / 2);

		_unitPos_L = _canvasMaxPos_L / divideFactor;
		_lowerBoundPos_L = _unitPos_L * (-1 * listBoxes.Length / 2 - 1);
		_upperBoundPos_L = _unitPos_L * (listBoxes.Length / 2 + 1);
		_shiftBoundPos_L = _unitPos_L * 0.3f;
        
		if ((listBoxes.Length & 0x1) == 0) {
			_lowerBoundPos_L += _unitPos_L / 2;
			_upperBoundPos_L -= _unitPos_L / 2;
		}
        
		if (!controlByButton) {
			foreach (Button button in buttons)
				button.gameObject.SetActive(false);
		}

        currentSelectedContent = 0;
    }
    
    void OnEnable() {
        metaGesture.PointerPressed += GesturePressed;
        metaGesture.PointerUpdated += GestureUpdated;
        metaGesture.PointerReleased += GestureReleased;
    }

    void OnDisable() {
        if (Base.isQuitting) return;
        metaGesture.PointerPressed -= GesturePressed;
        metaGesture.PointerUpdated -= GestureUpdated;
        metaGesture.PointerReleased -= GestureReleased;
    }

    private void GesturePressed(object sender, MetaGestureEventArgs e) {
        _lastInputPos_L = new Vector3(e.Pointer.Position.x, e.Pointer.Position.y);
        _lastInputPos_L /= _parentCanvas.scaleFactor;
        _startInputPos_L = _lastInputPos_L;
        _numofSlideFrames = 0;
        foreach (ListBox listBox in listBoxes)
            listBox.keepSliding = false;
    }

    private void GestureUpdated(object sender, MetaGestureEventArgs e) {
        _currentInputPos_L = new Vector3(e.Pointer.Position.x, e.Pointer.Position.y);
        _currentInputPos_L /= _parentCanvas.scaleFactor;
        _deltaInputPos_L = _currentInputPos_L - _lastInputPos_L;
        foreach (ListBox listbox in listBoxes)
            listbox.updatePosition(_deltaInputPos_L);

        _lastInputPos_L = _currentInputPos_L;
        ++_numofSlideFrames;
    }

    private void GestureReleased(object sender, MetaGestureEventArgs e) {
        setSlidingEffect();
    }
    
	void setSlidingEffect()
	{
		Vector3 deltaPos = _deltaInputPos_L;
		Vector3 slideDistance = _lastInputPos_L - _startInputPos_L;
		bool fastSliding = isFastSliding(_numofSlideFrames, slideDistance);

		if (fastSliding)
			deltaPos *= 5.0f;

		if (alignToCenter) {
			foreach (ListBox listbox in listBoxes) {
				listbox.setSlidingDistance(deltaPos, fastSliding ? slidingFrames >> 1 : slidingFrames >> 2);
				listbox.needToAlignToCenter = true;
            }
			_alignToCenterDistance = new Vector3(float.NaN, float.NaN, 0.0f);
		} else {
			foreach (ListBox listbox in listBoxes) {
				listbox.setSlidingDistance(deltaPos, fastSliding ? slidingFrames * 2 : slidingFrames);
            }
		}
        
        Debug.Log("Selected:" + CurrentSelectedRoom);
    }
    
	bool isFastSliding(int frames, Vector3 distance)
	{
		if (frames < 15) {
			switch (direction) {
			case Direction.HORIZONTAL:
				if (Mathf.Abs(distance.x) > _canvasMaxPos_L.x * 2.0f / 3.0f)
					return true;
				else
					return false;
			case Direction.VERTICAL:
				if (Mathf.Abs(distance.y) > _canvasMaxPos_L.y * 2.0f / 3.0f)
					return true;
				else
					return false;
			}
		}
		return false;
	}
    
	public Vector3 findDeltaPositionToCenter()
	{
		float minDeltaPos = Mathf.Infinity;
		float deltaPos;
        
		if (!float.IsNaN(_alignToCenterDistance.x) &&
			!float.IsNaN(_alignToCenterDistance.y))
			return _alignToCenterDistance;

		switch (direction) {
		case Direction.VERTICAL:
			foreach (ListBox listBox in listBoxes) {
				deltaPos = -listBox.transform.localPosition.y;
				if (Mathf.Abs(deltaPos) < Mathf.Abs(minDeltaPos))
					minDeltaPos = deltaPos;
			}

			_alignToCenterDistance = new Vector3(0.0f, minDeltaPos, 0.0f);
			break;

		case Direction.HORIZONTAL:
			foreach (ListBox listBox in listBoxes) {
				deltaPos = -listBox.transform.localPosition.x;
				if (Mathf.Abs(deltaPos) < Mathf.Abs(minDeltaPos))
					minDeltaPos = deltaPos;
			}

			_alignToCenterDistance = new Vector3(minDeltaPos, 0.0f, 0.0f);
			break;

		default:
			_alignToCenterDistance = Vector3.zero;
			break;
		}

		return _alignToCenterDistance;
	}
    
	Vector3 divideComponent(Vector3 a, Vector3 b)
	{
		return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
	}
    
	public void nextContent()
	{
		foreach (ListBox listbox in listBoxes)
			listbox.unitMove(1, true);
	}
    
	public void lastContent()
	{
		foreach (ListBox listbox in listBoxes)
			listbox.unitMove(1, false);
	}
}
