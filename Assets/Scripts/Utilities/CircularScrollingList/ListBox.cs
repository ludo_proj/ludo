﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ListBox : MonoBehaviour
{
	public int listBoxID;
	public TextMeshProUGUI content;

	public ListBox lastListBox;
	public ListBox nextListBox;

    private Image icon;

	private int _contentID;
    
	private Vector2 _boxMaxPos;
	private Vector2 _unitPos;
	private Vector2 _lowerBoundPos;
	private Vector2 _upperBoundPos;
	private Vector2 _shiftBoundPos;
	private float _positionAdjust;

	private Vector3 _slidingDistance;
	private Vector3 _slidingDistanceLeft;

	private Vector3 _originalLocalScale;

	private bool _keepSliding = false;
	private int _slidingFramesLeft;
	private bool _needToAlignToCenter = false;

	public bool keepSliding { set { _keepSliding = value; } }
	public bool needToAlignToCenter { set { _needToAlignToCenter = value; } }
    
	void Start()
	{
        icon = this.GetComponent<Image>();

        _boxMaxPos = ListPositionCtrl.Instance.canvasMaxPos_L * ListPositionCtrl.Instance.angularity;
		_unitPos = ListPositionCtrl.Instance.unitPos_L;
		_lowerBoundPos = ListPositionCtrl.Instance.lowerBoundPos_L;
		_upperBoundPos = ListPositionCtrl.Instance.upperBoundPos_L;
		_shiftBoundPos = ListPositionCtrl.Instance.shiftBoundPos_L;
		_positionAdjust = ListPositionCtrl.Instance.positionAdjust;

		_originalLocalScale = transform.localScale;

		initialPosition(listBoxID);
		initialContent();

        needToAlignToCenter = false;
    }

    void initialContent()
	{
		if (listBoxID == ListPositionCtrl.Instance.listBoxes.Length / 2)
			_contentID = 0;
		else if (listBoxID < ListPositionCtrl.Instance.listBoxes.Length / 2)
			_contentID = ListBank.Instance.getListLength() - (ListPositionCtrl.Instance.listBoxes.Length / 2 - listBoxID);
		else
			_contentID = listBoxID - ListPositionCtrl.Instance.listBoxes.Length / 2;

		while (_contentID < 0)
			_contentID += ListBank.Instance.getListLength();
		_contentID = _contentID % ListBank.Instance.getListLength();

		updateContent(ListBank.Instance.getListContent(_contentID));

        if (transform.localPosition.y > -10f && transform.localPosition.y < 10f) {
            ListPositionCtrl.Instance.currentSelectedContent = _contentID;
            icon.color = new Color(1f, 1f, 1f);
        } else {
            icon.color = new Color(.5f, .5f, .5f);
        }
    }

	void updateContent(string content)
	{
		this.content.text = content;
        icon.sprite = Base.Instance.allSprites[content.ToLower()];
        icon.SetNativeSize();
    }
    
	public void setSlidingDistance(Vector3 distance, int slidingFrames)
	{
		_keepSliding = true;
		_slidingFramesLeft = slidingFrames;

		_slidingDistanceLeft = distance;
		_slidingDistance = Vector3.Lerp(Vector3.zero, distance, ListPositionCtrl.Instance.slidingFactor);
	}
    
	public void unitMove(int unit, bool up_right)
	{
		Vector2 deltaPos;

		if (up_right)
			deltaPos = _unitPos * (float)unit;
		else
			deltaPos = _unitPos * (float)unit * -1;

		switch (ListPositionCtrl.Instance.direction) {
		case ListPositionCtrl.Direction.VERTICAL:
			setSlidingDistance(new Vector3(0.0f, deltaPos.y, 0.0f), ListPositionCtrl.Instance.slidingFrames);
			break;
		case ListPositionCtrl.Direction.HORIZONTAL:
			setSlidingDistance(new Vector3(deltaPos.x, 0.0f, 0.0f), ListPositionCtrl.Instance.slidingFrames);
			break;
		}
	}

	void Update()
	{
		if (_keepSliding) {
			--_slidingFramesLeft;
			if (_slidingFramesLeft == 0) {
				_keepSliding = false;
                
				if (_needToAlignToCenter) {
					setSlidingDistance(ListPositionCtrl.Instance.findDeltaPositionToCenter(),
						ListPositionCtrl.Instance.slidingFrames);
					_needToAlignToCenter = false;
					return;
				}
                
				if (ListPositionCtrl.Instance.alignToCenter ||
					ListPositionCtrl.Instance.controlByButton) {
					updatePosition(_slidingDistanceLeft);
				}
				return;
			}

			updatePosition(_slidingDistance);
			_slidingDistanceLeft -= _slidingDistance;
			_slidingDistance = Vector3.Lerp(Vector3.zero, _slidingDistanceLeft, ListPositionCtrl.Instance.slidingFactor);
		}
	}
    
	void initialPosition(int listBoxID)
	{
		if ((ListPositionCtrl.Instance.listBoxes.Length & 0x1) == 0) {
			switch (ListPositionCtrl.Instance.direction) {
			case ListPositionCtrl.Direction.VERTICAL:
				transform.localPosition = new Vector3(0.0f,
					_unitPos.y * (listBoxID * -1 + ListPositionCtrl.Instance.listBoxes.Length / 2) - _unitPos.y / 2,
					0.0f);
				updateXPosition();
				break;
			case ListPositionCtrl.Direction.HORIZONTAL:
				transform.localPosition = new Vector3(_unitPos.x * (listBoxID - ListPositionCtrl.Instance.listBoxes.Length / 2) - _unitPos.x / 2,
				0.0f, 0.0f);
				updateYPosition();
				break;
			}
		} else {
			switch (ListPositionCtrl.Instance.direction) {
			case ListPositionCtrl.Direction.VERTICAL:
				transform.localPosition = new Vector3(0.0f,
					_unitPos.y * (listBoxID * -1 + ListPositionCtrl.Instance.listBoxes.Length / 2),
					0.0f);
				updateXPosition();
				break;
			case ListPositionCtrl.Direction.HORIZONTAL:
				transform.localPosition = new Vector3(_unitPos.x * (listBoxID - ListPositionCtrl.Instance.listBoxes.Length / 2),
					0.0f, 0.0f);
				updateYPosition();
				break;
			}
		}
	}
    
	public void updatePosition(Vector3 deltaPosition_L)
	{
		switch (ListPositionCtrl.Instance.direction) {
		case ListPositionCtrl.Direction.VERTICAL:
			transform.localPosition += new Vector3(0.0f, deltaPosition_L.y, 0.0f);
			updateXPosition();
			checkBoundaryY();
            if (!_needToAlignToCenter) {
                if (transform.localPosition.y > -10f && transform.localPosition.y < 10f) {
                    ListPositionCtrl.Instance.currentSelectedContent = _contentID;
                    icon.color = new Color(1f, 1f, 1f);
                } else {
                    icon.color = new Color(.5f, .5f, .5f);
                }
            }
			break;
		case ListPositionCtrl.Direction.HORIZONTAL:
			transform.localPosition += new Vector3(deltaPosition_L.x, 0.0f, 0.0f);
			updateYPosition();
			checkBoundaryX();
			break;
		}
	}
    
	void updateXPosition()
	{
		transform.localPosition = new Vector3(
			_boxMaxPos.x * (_positionAdjust +
			Mathf.Cos(transform.localPosition.y / _upperBoundPos.y * Mathf.PI / 2.0f)),
			transform.localPosition.y, transform.localPosition.z);
		updateSize(_upperBoundPos.y, transform.localPosition.y);
	}
    
	void updateYPosition()
	{
		transform.localPosition = new Vector3(
			transform.localPosition.x,
			_boxMaxPos.y * (_positionAdjust +
			Mathf.Cos(transform.localPosition.x / _upperBoundPos.x * Mathf.PI / 2.0f)),
			transform.localPosition.z);
		updateSize(_upperBoundPos.x, transform.localPosition.x);
	}
    
	void checkBoundaryY()
	{
		float beyondPosY_L = 0.0f;
        
		if (transform.localPosition.y < _lowerBoundPos.y + _shiftBoundPos.y) {
			beyondPosY_L = (_lowerBoundPos.y + _shiftBoundPos.y - transform.localPosition.y);
			transform.localPosition = new Vector3(
				transform.localPosition.x,
				_upperBoundPos.y - _unitPos.y + _shiftBoundPos.y - beyondPosY_L,
				transform.localPosition.z);
			updateToLastContent();
		} else if (transform.localPosition.y > _upperBoundPos.y - _shiftBoundPos.y) {
			beyondPosY_L = (transform.localPosition.y - _upperBoundPos.y + _shiftBoundPos.y);
			transform.localPosition = new Vector3(
				transform.localPosition.x,
				_lowerBoundPos.y + _unitPos.y - _shiftBoundPos.y + beyondPosY_L,
				transform.localPosition.z);
			updateToNextContent();
		}

		updateXPosition();
	}

	void checkBoundaryX()
	{
		float beyondPosX_L = 0.0f;
        
		if (transform.localPosition.x < _lowerBoundPos.x + _shiftBoundPos.x) {
			beyondPosX_L = (_lowerBoundPos.x + _shiftBoundPos.x - transform.localPosition.x);
			transform.localPosition = new Vector3(
				_upperBoundPos.x - _unitPos.x + _shiftBoundPos.x - beyondPosX_L,
				transform.localPosition.y,
				transform.localPosition.z);
			updateToNextContent();
		} else if (transform.localPosition.x > _upperBoundPos.x - _shiftBoundPos.x) {
			beyondPosX_L = (transform.localPosition.x - _upperBoundPos.x + _shiftBoundPos.x);
			transform.localPosition = new Vector3(
				_lowerBoundPos.x + _unitPos.x - _shiftBoundPos.x + beyondPosX_L,
				transform.localPosition.y,
				transform.localPosition.z);
			updateToLastContent();
		}

		updateYPosition();
	}

	void updateSize(float smallest_at, float target_value)
	{
		transform.localScale = _originalLocalScale *
			(1.0f + ListPositionCtrl.Instance.scaleFactor * Mathf.InverseLerp(smallest_at, 0.0f, Mathf.Abs(target_value)));
    }

	public int getCurrentContentID()
	{
		return _contentID;
	}
    
	void updateToLastContent()
	{
		_contentID = nextListBox.getCurrentContentID() - 1;
		_contentID = (_contentID < 0) ? ListBank.Instance.getListLength() - 1 : _contentID;

		updateContent(ListBank.Instance.getListContent(_contentID));
	}
    
	void updateToNextContent()
	{
		_contentID = lastListBox.getCurrentContentID() + 1;
		_contentID = (_contentID == ListBank.Instance.getListLength()) ? 0 : _contentID;

		updateContent(ListBank.Instance.getListContent(_contentID));
	}

    public bool IsSelected() {
        Vector3 selectedScale = _originalLocalScale * (1.0f + ListPositionCtrl.Instance.scaleFactor);
        Debug.Log(transform.localScale + ", " + selectedScale + " : " + Mathf.Approximately(transform.localScale.x, selectedScale.x));
        return (transform.localScale == (_originalLocalScale *
            (1.0f + ListPositionCtrl.Instance.scaleFactor)));
    }
}
