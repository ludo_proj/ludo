﻿using UnityEngine;

public enum RoomName {
    Monas,
    LockedRoom
}

public class ListBank : MonoBehaviour
{
	public static ListBank Instance;

	private RoomName[] contents = {
		RoomName.Monas, RoomName.LockedRoom, RoomName.LockedRoom
    };

	void Awake()
	{
		Instance = this;
	}

	public string getListContent(int index)
	{
		return contents[index].ToString();
	}

	public int getListLength()
	{
		return contents.Length;
	}
}
