﻿using UnityEngine;

public static class Utilities {

    public static string AddCommas(int number) {
        return number.ToString("N");
    }

    public static bool HasComponent<T>(this GameObject go) where T : Component {
        return go.GetComponent<T>() != null;
    }

    public static string AddCommas(float number) {
        return number.ToString("N");
    }

    public static string ToSnakeCase(this string text) {
        return text.Replace(" ", "_");
    }

    public static string ToSentenceCase(this string input, bool allSentence = false) {
        if (input.Length < 1)
            return input;

        string[] values = input.Split(' ');
        for (int i = 0; i < values.Length; i++) {
            values[i] = values[i][0].ToString().ToUpper() + values[i].Substring(1);
            if (!allSentence) break;
        }

        return string.Join(" ", values);
    }

    public static RectTransform GetRectTransform(this GameObject go) {
        return go.GetComponent<RectTransform>();
    }

}
