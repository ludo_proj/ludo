using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class PlayerCanvasController : MonoBehaviour
{
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI money;
    public GameObject bubble;
    public Image emoticon;
    public GameObject timerGO;
    public TextMeshProUGUI timerText;

    public void setTitle(string value)
    {
        playerName.text = value;
    }

    public void setMoney(string value)
    {
        money.text = value;
    }

    public void setTurn(bool isUserTurn)
    {
        if (!isUserTurn) ShowTimer();
        GetComponent<RectTransform>().localScale = Vector3.one;
    }

    public void setIdle()
    {
        HideTimer();
        GetComponent<RectTransform>().localScale = new Vector3(.8f, .8f, 1);
    }

    public void TweenEmoticon(string emoticonStr) {
        emoticon.sprite = Base.Instance.allSprites["emoji_"+emoticonStr.ToLower()];
        bubble.SetActive(true);
        bubble.transform.DOScale(1f, .3f);
        DOVirtual.DelayedCall(5f, HideEmoticon);
    }

    private void HideEmoticon() {
        bubble.SetActive(false);
        bubble.transform.DOScale(0f, .3f);
    }

    public void UpdateTimer(float time) {
        timerText.text = "" + (int)time;
    }

    private void ShowTimer() {
        timerGO.SetActive(true);
        timerGO.transform.DOScale(1f, .3f);
    }

    private void HideTimer() {
        timerGO.SetActive(false);
        timerGO.transform.DOScale(0f, .3f);
    }
}