using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum EmoticonID {
        Angry,
        Laugh,
        Sad,
        Smile,
        Confused,
        Devil,
        Shocked,
        Thug
    }

    private List<SpriteRenderer> pawnShadows;
    
    public CameraController cameraController;
    public List<PawnController> pawnController;
    public int playerIdx;
    public bool isSelf;
    public List<PawnController> pawnCandidate;
    public GameController gameController;
    public Vector3[] pawnnPosition;
    public int totalFinishedPawn = 0;
    public PlayerCanvasController playerCanvasController;
    private int _money;
    public int money
    {
        get
        {
            return _money;
        }
        set
        {
            _money = value;
            playerCanvasController.setMoney(Utilities.AddCommas(_money));
        }
    }
    public bool gameOver = false;
    public bool leftRoom = false;

    void Awake()
    {
        pawnCandidate = new List<PawnController>();
        pawnController = new List<PawnController>();
        pawnShadows = new List<SpriteRenderer>();
        money = 10000;
        playerCanvasController.setMoney(Utilities.AddCommas(money));
    }

    void Start()
    {
    }

    public void init(Material material, int playerIdx, string playerName)
    {
        this.playerIdx = playerIdx;
        totalFinishedPawn = 0;
        playerCanvasController.setTitle(playerName);
        for (int i = 0; i < 4; i++)
        {
            GameObject go = Instantiate(gameController.pawnPrefab, pawnnPosition[i], Quaternion.Euler(0f,-90f,0f), transform);
            pawnController.Add(go.GetComponent<PawnController>());
            pawnController[i].transform.localPosition = pawnnPosition[i];
            pawnController[i].init(material, this, i);

            //create shadow here
            GameObject shadow = new GameObject("Shadow_" + playerIdx + "_" + i);
            SpriteRenderer sprRenderer = shadow.AddComponent<SpriteRenderer>();
            sprRenderer.sprite = Base.Instance.allSprites["emoji_round_bg"];
            sprRenderer.color = new Color(0f, 0f, 0f, .5f);
            shadow.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, go.transform.position.z);
            shadow.transform.rotation = Quaternion.Euler(90, 0, 0);
            shadow.transform.localScale = new Vector3(.6f, .6f, .6f);
            pawnShadows.Add(sprRenderer);
            pawnController[i].AssignShadow(sprRenderer);
        }
    }

    public int needToShowHighlight(int step)
    {
        pawnCandidate.Clear();
        int countCanMove = 0;
        for (int i = 0; i < pawnController.Count; i++)
        {
            if (pawnController[i].checkForHighlight(step))
            {
                pawnCandidate.Add(pawnController[i]);
                countCanMove++;
            }
        }
        if (countCanMove > 1)
        {
            Bounds bounds = new Bounds();
            for (int i = 0; i < pawnController.Count; i++)
            {
                if (pawnCandidate.IndexOf(pawnController[i]) > -1) {
                    bounds.Encapsulate(pawnController[i].meshRenderer.bounds);
                }
                pawnController[i].highlight(pawnCandidate.IndexOf(pawnController[i]) > -1);
            }
            cameraController.setBounds(bounds);
        }
        return countCanMove;
    }

    public int doStep(int step, Action callback)
    {
        // cameraController.setPosition(pawnCandidate[0].gameObject);
        pawnCandidate[0].doMove(step, callback);
        return pawnCandidate[0].index;
    }

    public void selectedPawn(PawnController pawn)
    {
        for (int i = 0; i < pawnController.Count; i++)
        {
            pawnController[i].clearHighlight();
        }
        cameraController.resetCamera();
        pawnCandidate.Clear();
        pawnCandidate.Add(pawn);
        gameController.doneSelected();
    }

    public void addFinishPawn()
    {
        totalFinishedPawn++;
        if (totalFinishedPawn == 4)
        {
            //Game Finish
            gameController.gameFinish(playerIdx);
        }
    }

    public void assignPath(Transform[] transform)
    {
        for (int i = 0; i < pawnController.Count; i++)
        {
            pawnController[i].path = transform;
        }
    }

    public void HideAllPawns() {
        for (int i = 0; i < pawnController.Count; i++) {
            pawnController[i].gameObject.SetActive(false);
            pawnShadows[i].gameObject.SetActive(false);
        }
    }

    public void ShowEmoticon(string emoticonStr) {
        //PlayerController.EmoticonID emoticonID = (PlayerController.EmoticonID)Enum.Parse(typeof(PlayerController.EmoticonID), emoticonStr);
        playerCanvasController.TweenEmoticon(emoticonStr);
    }
}