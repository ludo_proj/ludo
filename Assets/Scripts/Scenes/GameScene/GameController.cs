using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using UnityEngine.SceneManagement;
using System;
using TouchScript.Gestures.TransformGestures;

public class GameController : Photon.MonoBehaviour
{
    public enum PlayerColor {
        Green,
        Red,
        Blue,
        Yellow
    }

    public const float TURN_TIME = 12f;

    public GameObject emojiContainer;
    public GameObject pawnPrefab;

    public CameraController cameraController;
    public DiceController diceController;
    public PlayerController[] players;
    public Transform[] redPath;
    public Transform[] bluePath;
    public Transform[] yellowPath;
    public Transform[] greenPath;
    public Material[] materialColor;
    public Button button;
    public Image diceTimer;
    public TransformGesture transformGesture;

    //my data
    public int playerIdx;
    public int rollSixCount = 0;
    public int currentPlayerCount;
    public int step;
    public int currentTurn;
    public float timeLeft;
    public bool gameStarted = false;
    public bool gameFinished = false;
    public bool pauseTime = false;

    bool isShowingEmoticon;
    public bool isAlive = true;
    float lerpSmoothing = 5f;
    float canvasScaleFactor;

    void Awake()
    {
        Base.Instance.Init();
        button.interactable = false;
    }

    void Start()
    {
        canvasScaleFactor = GameObject.Find("Canvas").GetComponent<Canvas>().scaleFactor;

        PhotonNetwork.RaiseEvent((int)EnumPhoton.Ready, null, true, null);
        checkReady();
    }

    private void OnApplicationPause(bool pause)
    {
        if (SceneManager.GetActiveScene().name == SceneController.SceneID.GameScreen.ToString())
        {
            if (!pause)
            {
                StartCoroutine(ReconnectAndRejoinCor());
            }
        }
    }

    IEnumerator ReconnectAndRejoinCor()
    {
        yield return new WaitForEndOfFrame();

        if (!PhotonNetwork.connected)
        {
            Base.Instance.textController.updateText("Mau Masuk Balik");
            ReconnectAndRejoin();
        }
    }

    public void ReconnectAndRejoin()
    {
        PhotonNetwork.ConnectUsingSettings(NetworkManager.VERSION);
    }

    void OnJoinedLobby()
    {
        Base.Instance.networkManager.joinRoom(Base.Instance.networkManager.roomName);
    }

    void OnJoinedRoom()
    {
        Base.Instance.textController.updateText("Uda Masuk Balik");
        currentTurn = PhotonNetwork.room.GetTurn();
        setPlayerCanvasUI(true);
        players[playerIdx].leftRoom = false;
        if (currentTurn == playerIdx)
        {
            setEnableRollDice();
        }
    }

    private PhotonPlayer newMaster;
    private void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        newMaster = newMasterClient;
        Debug.Log("New Master:" + newMasterClient.NickName);
    }

    private void OnPlayerDisconnected(NetworkPlayer player)
    {

    }

    public void StartGame()
    {
        if (gameStarted) return;
        playerIdx = Base.Instance.networkManager.getPlayerIdx();
        currentPlayerCount = PhotonNetwork.playerList.Length;
        for (int i = 0; i < players.Length; i++)
        {
            if (i >= currentPlayerCount)
                players[i].init(materialColor[i], i + 1, "None");
            else
                players[i].init(materialColor[i], i + 1, PhotonNetwork.playerList[i].NickName);
            if ((i + 1) == playerIdx)
            {
                players[i].isSelf = true;
            }
            if (i >= currentPlayerCount) {
                players[i].gameOver = true;
                players[i].playerCanvasController.gameObject.SetActive(false);
                players[i].HideAllPawns();
            }
        }
        players[0].assignPath(greenPath);
        players[1].assignPath(redPath);
        players[2].assignPath(bluePath);
        players[3].assignPath(yellowPath);
        button.image.sprite = Base.Instance.allSprites["dice_btn_" + ((PlayerColor)playerIdx-1).ToString().ToLower()];
        currentTurn = 1;
        if (currentTurn == playerIdx)
        {
            PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, playerIdx, true, null);
            setRoomProperties();
            nextTurn(playerIdx);
            setTimeLeft();
        }
        setPlayerCanvasUI();
        gameStarted = true;
    }

    public void nextTurn(int turn)
    {
        button.interactable = false;
        currentTurn = turn;
        if (currentTurn % (currentPlayerCount + 1) == 0)
        {
            currentTurn = 1;
        }
        // Base.Instance.textController.updateText("Turn " + currentTurn);
        setPlayerCanvasUI();
    }

    public void setPlayerCanvasUI(bool fromReconnect = false)
    {
        ExitGames.Client.Photon.Hashtable currentProperties = PhotonNetwork.room.CustomProperties;
        for (int i = 0; i < players.Length; i++)
        {
            players[i].playerCanvasController.setIdle();
            if (fromReconnect)
            {
                players[i].money = (int)currentProperties["money" + (i + 1)];
            }
        }
        // if (currentTurn == playerIdx)
        // {
        players[currentTurn - 1].playerCanvasController.setTurn(currentTurn == playerIdx);
        setTimeLeft();
        setRoomProperties();
        // }
        StartCoroutine(ShowTurn());
    }

    public void setEnableRollDice()
    {
        button.interactable = true;
    }

    public void setDisableRollDice()
    {
        button.interactable = false;
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        PhotonNetwork.OnEventCall += onEventCall;
        transformGesture.Transformed += onTransform;
    }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive.
    /// </summary>
    void OnDisable()
    {
        PhotonNetwork.OnEventCall -= onEventCall;
        transformGesture.Transformed -= onTransform;
    }

    private void onTransform(object sender, System.EventArgs e)
    {
        if (transformGesture.NumPointers == 1)
        {
            cameraController.updateGesturePosition(transformGesture.DeltaPosition);
        }
        if (transformGesture.NumPointers == 2)
        {
            if (transformGesture.DeltaScale > 1)
            {
                cameraController.updateGestureScale(-1.5f);
            }
            else
            {
                cameraController.updateGestureScale(1.5f);
            }
        }
    }

    public void onClick(string value)
    {
        // Base.Instance.textController.updateText("Clicked: " + value);
        switch (value)
        {
            case "roll":
                PhotonNetwork.RaiseEvent((int)EnumPhoton.PauseTime, null, true, null);
                pauseTime = true;
                photonView.TransferOwnership(PhotonNetwork.player);
                step = UnityEngine.Random.Range(1, 7);
                diceController.rollDice(step, checkForMovePawn);
                // Base.Instance.textController.updateText("Step: " + step);
                button.interactable = false;
                break;
            case "setting":
                WindowManager.ShowWindow("WSetting");
                break;
            case "chat":
                if (!isShowingEmoticon) {
                    isShowingEmoticon = true;
                    emojiContainer.transform.DOMoveY(130f * canvasScaleFactor, .3f);
                } else {
                    //emojiContainer.transform.DOMoveY(-150f, .3f);
                    //isShowingEmoticon = false;
                }
                break;
            case "close_emoji":
                emojiContainer.transform.DOMoveY(-150f * canvasScaleFactor, .3f);
                isShowingEmoticon = false;
                break;
            default:
                if (value.IndexOf("emo") > -1)
                {
                    string expression = value.Split('_')[1];
                    PlayerController.EmoticonID emoticonID = (PlayerController.EmoticonID)Enum.Parse(typeof(PlayerController.EmoticonID), expression.ToSentenceCase());
                    SendEmoticon(emoticonID);
                    emojiContainer.transform.DOMoveY(-150f, .3f);
                    isShowingEmoticon = false;
                }
                break;
        }
    }

    private void checkForMovePawn()
    {
        PhotonNetwork.RaiseEvent((int)EnumPhoton.Continue, null, true, null);
        pauseTime = false;
        if (step == 6)
        {
            rollSixCount++;
            if (rollSixCount >= 3)
            {
                // Skip move
                rollSixCount = 0;
                currentTurn = playerIdx;
                do
                {
                    currentTurn++;
                    if (currentTurn % (currentPlayerCount + 1) == 0)
                    {
                        currentTurn = 1;
                    }
                    if (!players[currentTurn - 1].gameOver && !players[currentTurn - 1].leftRoom) break;
                } while (true);
                PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, currentTurn, true, null);
                setPlayerCanvasUI();
                return;
            }
        }
        else
        {
            rollSixCount = 0;
        }
        int CountPawn = players[playerIdx - 1].needToShowHighlight(step);
        // Base.Instance.textController.updateText("count Pawn: " + CountPawn);
        if (CountPawn > 0)
        {
            if (CountPawn > 1)
            {
                //Wait for user click
                transformGesture.gameObject.SetActive(false);
                PhotonNetwork.RaiseEvent((int)EnumPhoton.PauseTime, null, true, null);
                pauseTime = true;
            }
            else
            {
                int pawnIdx = players[playerIdx - 1].doStep(step, doMovePawn);
                string data = step + ";" + playerIdx + ";" + pawnIdx;
                PhotonNetwork.RaiseEvent((int)EnumPhoton.Move, data, true, null);
            }
        }
        else
        {
            currentTurn = playerIdx;
            do
            {
                if (step < 6) currentTurn++;
                if (currentTurn % (currentPlayerCount + 1) == 0)
                {
                    currentTurn = 1;
                }
                if (!players[currentTurn - 1].gameOver && !players[currentTurn - 1].leftRoom) break;
            } while (true);
            PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, currentTurn, true, null);
            setPlayerCanvasUI();
        }
    }

    public void doneSelected()
    {
        transformGesture.gameObject.SetActive(true);
        PhotonNetwork.RaiseEvent((int)EnumPhoton.Continue, null, true, null);
        pauseTime = false;
        int pawnIdx = players[playerIdx - 1].doStep(step, doMovePawn);
        string data = step + ";" + playerIdx + ";" + pawnIdx;
        PhotonNetwork.RaiseEvent((int)EnumPhoton.Move, data, true, null);
    }

    public void doMovePawn()
    {
        if (step == 6)
        {
            PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, playerIdx, true, null);
            setRoomProperties();
            nextTurn(playerIdx);
        }
        else
        {
            currentTurn = playerIdx;
            do
            {
                currentTurn++;
                if (currentTurn % (currentPlayerCount + 1) == 0)
                {
                    currentTurn = 1;
                }
                if (!players[currentTurn - 1].gameOver && !players[currentTurn - 1].leftRoom) break;
            } while (true);
            PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, currentTurn, true, null);
            setPlayerCanvasUI();
        }
    }

    public void makeOtherMove(int _step, int playerIdx, int pawnIdx)
    {
        players[playerIdx - 1].pawnController[pawnIdx].doMove(_step, cameraController.setToDice);
    }

    public void gameFinish(int index)
    {
        //gameFinished = true;
        // PhotonNetwork.RaiseEvent((int)EnumPhoton.Finish, playerIdx, true, null);
        // EndGame();
        //Show Window in here
        players[index-1].gameOver = true;
        bool playerWon = (players[this.playerIdx-1].totalFinishedPawn >= 4);
        int totalPlayerDone = 0;
        for (int i = 0; i < players.Length; i++) {
            if (players[i].gameOver) {
                totalPlayerDone++;
            }
        }
        if (this.playerIdx == index) {
            //WindowManager.ShowWindow("WGameWin");
            //if (playerWon) {
                Base.Instance.textController.updateText("Player " + playerIdx + " Win!!");
                Base.Instance.stickerManager.ShowSticker(StickerManager.StickerID.PlayerWin, new StickerManager.StickerData {
                    sourceIdx = this.playerIdx,
                    winRank = GetTotalFinishedPlayers()
                });
            //}
        } else {
            if (!playerWon && totalPlayerDone == 3) {
                Base.Instance.stickerManager.ShowSticker(StickerManager.StickerID.PlayerLose, new StickerManager.StickerData {
                    sourceIdx = this.playerIdx,
                });
            }
        }
        //else
        //    WindowManager.ShowWindow("WGameOver", new WGameOver.GameOverData { title = "Game Over", desc = "Player " + playerIdx + " Win!!" });
    }

    // public void EndGame()
    // {
    // }

    public void checkReady()
    {
        ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.room.CustomProperties;
        // Base.Instance.textController.updateText(hashtable["totalReady"]);
        if (PhotonNetwork.isMasterClient)
        {
            //if ((int)hashtable["totalReady"] == 4)
            //{
                StartGame();
                PhotonNetwork.RaiseEvent((int)EnumPhoton.StartGame, null, true, null);
                ExitGames.Client.Photon.Hashtable currentProperties = PhotonNetwork.room.CustomProperties;
                for (int i = 0; i < currentPlayerCount; i++)
                {
                    currentProperties["money" + (i + 1)] = players[i].money;
                }
                PhotonNetwork.room.SetCustomProperties(currentProperties);
            //}
        }
    }

    void onEventCall(byte eventcode, object content, int senderid)
    {
        /*if (eventcode == (int)EnumPhoton.JoinedRoom)
        {
            if (Base.Instance.networkManager.PlayerIsRequired)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    //PhotonNetwork.room.IsOpen = false;
                    PhotonNetwork.room.IsVisible = false;
                    StartGame();
                    PhotonNetwork.RaiseEvent((int)EnumPhoton.StartGame, null, true, null);
                }
            }
        }
        else*/ if (eventcode == (int)EnumPhoton.Ready)
        {
            checkReady();
        }
        else if (eventcode == (int)EnumPhoton.StartGame)
        {
            StartGame();
        }
        else if (eventcode == (int)EnumPhoton.NextPlayerTurn)
        {
            int playerTurn = (int)content;
            button.interactable = false;
            nextTurn(playerTurn);
        }
        else if (eventcode == (int)EnumPhoton.Finish)
        {
            //Game Finished
        }
        else if (eventcode == (int)EnumPhoton.Pay)
        {
            Base.Instance.textController.updateText("eventCode: " + ((EnumPhoton)eventcode).ToString() + " content: " + content + " senderid: " + senderid);
            //Pay The Player
            string[] data = ((string)content).Split(';');
            int playerToPay = int.Parse(data[0]) - 1;
            int count = int.Parse(data[1]);
            int targetPlayer = int.Parse(data[2]) - 1;
            int toPay = count;
            if (toPay > players[playerToPay].money)
            {
                toPay = players[playerToPay].money;
            }
            players[playerToPay].money -= toPay;
            if (players[playerToPay].money == 0)
            {
                //Game Over
                players[playerToPay].gameOver = true;
                if (playerToPay + 1 == playerIdx)
                {
                    PhotonNetwork.RaiseEvent((int)EnumPhoton.GameOver, playerToPay, true, null);
                    //Show self gameOver
                    Base.Instance.stickerManager.ShowSticker(StickerManager.StickerID.PlayerLose, new StickerManager.StickerData {
                        sourceIdx = playerIdx,
                    });
                    //WindowManager.ShowWindow("WGameOver", new WGameOver.GameOverData { title = "Game Over", desc = "You have spent all of your money." });
                }
            }
            if (targetPlayer + 1 == playerIdx)
            {
                players[targetPlayer].money += toPay;
                Base.Instance.textController.updateText("Call Receive: " + playerIdx);
                PhotonNetwork.RaiseEvent((int)EnumPhoton.ReceiveMoney, targetPlayer + ";" + toPay, true, null);
            }
        }
        else if (eventcode == (int)EnumPhoton.ReceiveMoney)
        {
            Base.Instance.textController.updateText("eventCode: " + ((EnumPhoton)eventcode).ToString() + " content: " + content + " senderid: " + senderid);
            string[] data = ((string)content).Split(';');
            int playerToPay = int.Parse(data[0]);
            int money = int.Parse(data[1]);
            // if (playerToPay == playerIdx)
            // {
            players[playerToPay].money += money;
            //Add Money
            // }
        }
        else if (eventcode == (int)EnumPhoton.Move)
        {
            string[] data = ((string)content).Split(';');
            int step = int.Parse(data[0]);
            int pl = int.Parse(data[1]);
            int pawnIdx = int.Parse(data[2]);
            makeOtherMove(step, pl, pawnIdx);
        }
        else if (eventcode == (int)EnumPhoton.PauseTime)
        {
            pauseTime = true;
        }
        else if (eventcode == (int)EnumPhoton.Continue)
        {
            pauseTime = false;
        }
        else if (eventcode == (int)EnumPhoton.GameOver)
        {
            int playerId = (int)content;
            players[playerId].gameOver = true;
            int playerLeft = 0;
            for (int i = 0; i < players.Length; i++)
            {
                if (!players[i].gameOver || !players[i].leftRoom)
                {
                    playerLeft++;
                }
            }
            if (playerLeft == 1)
            {
                gameFinish(playerIdx);
            }
        }
        else if (eventcode == (int)EnumPhoton.SendChatEmojiMessage)
        {
        }
    }
    
    void Update()
    {
        if (PhotonNetwork.isMasterClient && timeLeft > 0 && gameStarted && !pauseTime && !gameFinished && !Base.Instance.stickerManager.isShowing)
        {
            timeLeft -= Time.deltaTime;
            photonView.RPC("TurnTimeLeft_RPC", PhotonTargets.All, timeLeft);
            if (timeLeft <= 0)
            {
                do
                {
                    currentTurn++;
                    if (currentTurn % (currentPlayerCount + 1) == 0)
                    {
                        currentTurn = 1;
                    }
                    if (!players[currentTurn - 1].gameOver && !players[currentTurn - 1].leftRoom) break;
                } while (true);
                Base.Instance.textController.updateText("next turn" + currentTurn);
                PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, currentTurn, true, null);
                nextTurn(currentTurn);
                setPlayerCanvasUI();
                setTimeLeft();
            }
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
        }
        else if (Input.GetKeyDown(KeyCode.S)) {
        }
        else if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.C) ||
        Input.GetKeyDown(KeyCode.V) || Input.GetKeyDown(KeyCode.B) || Input.GetKeyDown(KeyCode.N))
        {
            PhotonNetwork.RaiseEvent((int)EnumPhoton.PauseTime, null, true, null);
            pauseTime = true;
            photonView.TransferOwnership(PhotonNetwork.player);
            if (Input.GetKeyDown(KeyCode.Z))
            {
                step = 1;
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                step = 2;
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                step = 3;
            }
            else if (Input.GetKeyDown(KeyCode.V))
            {
                step = 4;
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                step = 5;
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                step = 6;
            }
            diceController.rollDice(step, checkForMovePawn);
            // Base.Instance.textController.updateText("Step: " + step);
            button.interactable = false;
        }

    }

    void OnPhotonPlayerConnected(PhotonPlayer otherPlayer)
    {
        Debug.Log("PlayerMasuk:" + otherPlayer.NickName);
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        int idx = 0;
        if (PhotonNetwork.isMasterClient)
        {
            for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
            {
                if (PhotonNetwork.playerList[i].ID == otherPlayer.ID)
                {
                    idx = i + 1;
                    break;
                }
            }

            photonView.RPC("Leave_Room_RPC", PhotonTargets.All, idx);
        }
    }

    void OnLeftRoom()
    {
        photonView.RPC("Leave_Room_RPC", PhotonTargets.All, playerIdx);
    }

    void setTimeLeft()
    {
        if (PhotonNetwork.isMasterClient)
        {
            timeLeft = TURN_TIME;
        }
    }

    [PunRPC]
    void Leave_Room_RPC(int leavePlayerIdx)
    {
        leaveRoom(leavePlayerIdx);
    }

    [PunRPC]
    void TurnTimeLeft_RPC(float time)
    {
        timeLeft = time;
        if (currentTurn % (currentPlayerCount + 1) == 0) return;
        diceTimer.gameObject.SetActive(currentTurn == playerIdx);
        UpdateDiceBar(time / TURN_TIME);
        players[currentTurn - 1].playerCanvasController.UpdateTimer(time);
    }

    private void leaveRoom(int leavePlayerIdx)
    {
        Base.Instance.textController.updateText("player Leave Idx : " + leavePlayerIdx);
        players[leavePlayerIdx - 1].leftRoom = true;
        if (PhotonNetwork.isMasterClient)
        {
            if (currentTurn == leavePlayerIdx)
            {
                currentTurn = leavePlayerIdx;
                do
                {
                    currentTurn++;
                    if (currentTurn % (currentPlayerCount+1) == 0)
                    {
                        currentTurn = 1;
                    }
                    if (!players[currentTurn - 1].gameOver && !players[currentTurn - 1].leftRoom) break;
                } while (true);
                PhotonNetwork.RaiseEvent((int)EnumPhoton.NextPlayerTurn, currentTurn, true, null);
                setPlayerCanvasUI();
                if (currentTurn == playerIdx)
                    setEnableRollDice();
            }
        }
        int playerLeft = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (!players[i].gameOver || !players[i].leftRoom)
            {
                playerLeft++;
            }
        }
        if (playerLeft == 1)
        {
            gameFinish(playerIdx);
        }
    }

    private void setRoomProperties()
    {
        ExitGames.Client.Photon.Hashtable currentProperties = PhotonNetwork.room.CustomProperties;
        currentProperties["currentTurn"] = currentTurn;
        PhotonNetwork.room.SetCustomProperties(currentProperties);
        PhotonNetwork.room.SetTurn(currentTurn);
        // Base.Instance.textController.updateText("room Current Turn: " + currentTurn);
    }

    public void updatePlayerMoneyInRoom(int player1Idx, int player2Idx, int total)
    {
        ExitGames.Client.Photon.Hashtable currentProperties = PhotonNetwork.room.CustomProperties;
        currentProperties["money" + player1Idx] = players[player1Idx - 1].money - total;
        currentProperties["money" + player2Idx] = players[player2Idx - 1].money + total;
        PhotonNetwork.room.SetCustomProperties(currentProperties);
    }

    IEnumerator ShowTurn()
    {
        Base.Instance.stickerManager.ShowSticker(StickerManager.StickerID.PlayerTurn, new StickerManager.StickerData {
            sourceIdx = currentTurn
        });

        while (Base.Instance.stickerManager.isShowing)
            yield return null;

        cameraController.setToDice();
        if (currentTurn == playerIdx) {
            setEnableRollDice();
        }
    }

    [PunRPC]
    void ShowEmoticon(int playerIdx, string emoticonStr)
    {
        Base.Instance.textController.updateText("Player " + playerIdx + ":" + emoticonStr);
        players[playerIdx - 1].ShowEmoticon(emoticonStr);
    }

    public void SendEmoticon(PlayerController.EmoticonID emoticon)
    {
        photonView.RPC("ShowEmoticon", PhotonTargets.All, new object[] { playerIdx, emoticon.ToString() });
    }

    public void UpdateDiceBar(float time) {
        diceTimer.fillAmount = time;
    }

    public int GetTotalFinishedPlayers() {
        int result = 0;
        for (int i = 0; i < players.Length; i++) {
            if (players[i].totalFinishedPawn >= 4) {
                result++;
            }
        }
        return result;
    }
}