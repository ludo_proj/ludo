using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TouchScript.Gestures;
using UnityEngine;

public class PawnController : MonoBehaviour
{
    public int index;
    public Transform[] path;
    public int currentPathIdx;
    public bool isFinish;
    public bool isHome;
    public bool isInBoard;
    public bool clickAble;
    public PlayerController playerController;
    public MeshRenderer meshRenderer;

    //Arrow
    public GameObject arrow;
    private SpriteRenderer arrowRenderer;
    private Animator arrowAnimator;

    private SpriteRenderer pawnShadow;

    public TapGesture tapGesture;

    public void init(Material material, PlayerController playerController, int index)
    {
        this.index = index;
        isFinish = false;
        isHome = true;
        isInBoard = false;
        Material[] mats = meshRenderer.materials;
        mats[0] = material;
        meshRenderer.materials = mats;
        this.playerController = playerController;
    }

    public bool checkForHighlight(int step)
    {
        clearHighlight();
        if (isFinish) return false;
        if (step == 6)
        {
            if (isHome)
            {
                // highlight();
                return true;
            }
        }
        if (currentPathIdx + step <= path.Length - 1)
        {
            if (isInBoard)
            {
                // highlight();
                return true;
            }
        }
        return false;
    }

    public void highlight(bool movable)
    {
        clickAble = movable;
        arrow.SetActive(true);
        arrowRenderer.sprite = Base.Instance.allSprites[movable?"movable_btn":"unmovable_btn"];
        arrow.transform.LookAt(Camera.main.transform.position, Vector3.up);
    }

    public void clearHighlight()
    {
        clickAble = false;
        arrow.SetActive(false);
    }

    public void doMove(int step, Action callback = null)
    {
        meshRenderer.enabled = true;
        playerController.cameraController.setPosition(gameObject);
        if (isInBoard)
        {
            TileObj tileObj = path[currentPathIdx].GetComponent<TileObj>();
            tileObj.removePawn(this);
            int fromStackCount = 0;
            if (tileObj.getCurrentTilePawn().Count > 1)
            {
                fromStackCount = tileObj.getCurrentTilePawn().Count + 1;
            }
            StartCoroutine(animateMove(step, currentPathIdx, fromStackCount, callback));
            currentPathIdx += step;
            // transform.position = path[currentPathIdx].transform.position;
            // transform.rotation = Quaternion.Euler(0, path[currentPathIdx].rotation.eulerAngles.y, 0);
            // path[currentPathIdx].GetComponent<TileObj>().addPawn(this, fromStackCount);
        }
        else
        {
            currentPathIdx = 0;
            transform.position = path[currentPathIdx].transform.position + new Vector3(0, 0.125f, 0);
            transform.rotation = Quaternion.Euler(0, path[currentPathIdx].GetComponent<TileObj>().direction, 0);
            isInBoard = true;
            isHome = false;
            path[currentPathIdx].GetComponent<TileObj>().addPawn(this, 0);
            if (callback != null)
            {
                callback();
            }
        }
        if (currentPathIdx == path.Length - 1)
        {
            isInBoard = false;
            isFinish = true;
            playerController.addFinishPawn();
            if (callback != null)
            {
                callback();
            }
        }
    }

    IEnumerator animateMove(int count, int from, int fromStackCount, Action callback = null)
    {
        List<TileObj> highlightTile = new List<TileObj>();
        for (int i = 0; i < count; i++) {
            TileObj tile = path[from+i+1].GetComponent<TileObj>();
            tile.SetGlow();
            highlightTile.Add(tile);
        }

        yield return new WaitForSeconds(.3f);

        float duration = .2f;
        for (int i = 0; i < count; i++)
        {
            Vector3 offsetPos = new Vector3(0, 0.125f, 0);
            if ((from + i + 1) >= path.Length - 1) {
                TileObj tile = path[currentPathIdx].GetComponent<TileObj>();
                offsetPos = tile.GetFinishLineOffset();
            }

            float startTime = 0;
            Vector3 fromPos = path[from + i].transform.position + offsetPos;
            Vector3 toPos = path[from + i + 1].transform.position + offsetPos;
            Vector3[] paths = new Vector3[] { fromPos, (fromPos + toPos) / 2 + new Vector3(0, 2.5f, 0), toPos };
            transform.DOPath(paths, .5f, PathType.CatmullRom).OnComplete(highlightTile[i].SetToNormal);
            pawnShadow.DOFade(0f, .1f).OnComplete(() => { pawnShadow.DOFade(.5f, .05f).SetDelay(.3f); });
            while (startTime < duration)
            {
                // transform.position = Vector3.Lerp(path[from + i].transform.position, path[from + i + 1].transform.position, startTime / duration);
                transform.rotation = Quaternion.Lerp(Quaternion.Euler(0, path[from + i].GetComponent<TileObj>().direction, 0),
                                                    Quaternion.Euler(0, path[from + i + 1].GetComponent<TileObj>().direction, 0), startTime / duration);
                startTime += Time.deltaTime;
                yield return null;
            }

            yield return null;
        }
        path[currentPathIdx].GetComponent<TileObj>().addPawn(this, fromStackCount);

        highlightTile.Clear();

        yield return null;
        if (callback != null)
        {
            callback();
        }
    }

    public void resetPosition()
    {
        currentPathIdx = -1;
        isInBoard = false;
        isHome = true;
        isFinish = false;
        transform.localPosition = playerController.pawnnPosition[index];
        transform.rotation = Quaternion.identity;
    }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        tapGesture = arrow.GetComponent<TapGesture>();
        arrowRenderer = arrow.GetComponent<SpriteRenderer>();
        arrowAnimator = arrow.GetComponent<Animator>();
    }

    void Start()
    {

    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        tapGesture.Tapped += OnTap;
    }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive.
    /// </summary>
    void OnDisable()
    {
        tapGesture.Tapped -= OnTap;
    }

    void OnTap(object sender, EventArgs e)
    {
        if (!clickAble) return;
        Base.Instance.textController.updateText("clicked " + name);
        playerController.selectedPawn(this);
    }

    public void AssignShadow(SpriteRenderer spr) {
        pawnShadow = spr;
    }

    void LateUpdate() {
        if (pawnShadow) {
            pawnShadow.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }    
    }

}