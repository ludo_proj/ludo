using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObj : MonoBehaviour
{
    public bool isSafe;
    public List<PawnController> pawnControllers;
    public float direction;

    MeshRenderer meshRenderer;

    CharacterStack characterStack;

    void Start()
    {
        pawnControllers = new List<PawnController>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    public void addPawn(PawnController pawnController, int totalFromStack)
    {
        StartCoroutine(AddPawnCor(pawnController, totalFromStack));
    }

    public void removePawn(PawnController pawnController)
    {
        for (int i = 0; i < pawnControllers.Count; i++)
        {
            if (pawnController.playerController.playerIdx == pawnControllers[i].playerController.playerIdx)
            {
                if (pawnController.index == pawnControllers[i].index)
                {
                    pawnControllers.RemoveAt(i);
                    break;
                }
            }
        }

        if (isSafe) {
            CreateCharacterStack();
        }
    }

    public bool containsPawn()
    {
        return pawnControllers.Count > 1;
    }

    public List<PawnController> getCurrentTilePawn()
    {
        return pawnControllers;
    }

    public bool isKickOtherPlayer(PawnController pawnController)
    {
        if (isSafe) return false;
        for (int i = 0; i < pawnControllers.Count; i++)
        {
            Base.Instance.textController.updateText(pawnController.playerController.playerIdx + "," + pawnControllers[i].playerController.playerIdx);
            if (pawnController.playerController.playerIdx != pawnControllers[i].playerController.playerIdx)
            {
                return true;
            }
        }
        return false;
    }

    public int kickAllPawn()
    {
        int playerIdx = 0;
        for (int i = 0; i < pawnControllers.Count; i++)
        {
            playerIdx = pawnControllers[i].playerController.playerIdx;
            pawnControllers[i].resetPosition();
        }
        pawnControllers.Clear();
        return playerIdx;
    }

    public int GetKickedIndex() {
        int playerIdx = 0;
        for (int i = 0; i < pawnControllers.Count; i++) {
            playerIdx = pawnControllers[i].playerController.playerIdx;
        }
        return playerIdx;
    }

    public void SetGlow() {
        Color color = new Color(1f, 1f, 1f);
        meshRenderer.material.SetColor("_EmissionColor", color);
    }

    public void SetToNormal() {
        Color color = new Color(.5f, .5f, .5f);
        meshRenderer.material.SetColor("_EmissionColor", color);
    }

    public Vector3 GetFinishLineOffset() {
        if (pawnControllers.Count == 3)
            return new Vector3(0.55f, 0.65f, -0.55f);
        else if (pawnControllers.Count == 2)
            return new Vector3(-0.55f, 0.65f, -0.55f);
        else if (pawnControllers.Count == 1)
            return new Vector3(0.55f, 0.65f, 0.55f);
        else
            return new Vector3(-0.55f, 0.65f, 0.55f);
    }

    IEnumerator AddPawnCor(PawnController pawnController, int totalFromStack) {
        if (isKickOtherPlayer(pawnController)) {
            totalFromStack += getCurrentTilePawn().Count;
            int playerIdx = GetKickedIndex();

            Base.Instance.stickerManager.ShowSticker(StickerManager.StickerID.PlayerKick, new StickerManager.StickerData {
                sourceIdx = pawnController.playerController.playerIdx,
                targetIdx = playerIdx
            });

            while (Base.Instance.stickerManager.isShowing) {
                yield return null;
            }

            kickAllPawn();

            PlayerController playerController = pawnController.playerController.gameController.players[playerIdx - 1];
            int toPay = totalFromStack * 100;
            if (toPay > playerController.money) {
                toPay = playerController.money;
            }
            if (playerController.money - toPay == 0) {
                playerController.gameOver = true;
            }
            if (playerIdx == Base.Instance.networkManager.getPlayerIdx()) {
                playerController.money -= toPay;
                Base.Instance.textController.updateText("Call Pay: " + playerIdx + "-" + pawnController.playerController.playerIdx);
                pawnController.playerController.gameController.updatePlayerMoneyInRoom(playerIdx, pawnController.playerController.playerIdx, toPay);
                PhotonNetwork.RaiseEvent((int)EnumPhoton.Pay, playerIdx + ";" + toPay + ";" + pawnController.playerController.playerIdx, true, null);
            }
        }

        pawnControllers.Add(pawnController);

        yield return new WaitForSeconds(.5f);

        if (isSafe) {
            CreateCharacterStack();
        }

        yield return null;
    }

    void CreateCharacterStack() {
        if (characterStack != null) {
            DestroyImmediate(characterStack.gameObject);
            //reset to show all pawns
            for (int i = 0; i < pawnControllers.Count; i++) {
                pawnControllers[i].meshRenderer.enabled = true;
            }
        }
        if (pawnControllers.Count <= 1) return;
        characterStack = (Instantiate(Resources.Load("Prefabs/char_stack " + Mathf.Min(pawnControllers.Count-1,3)), new Vector3(this.transform.position.x, this.transform.position.y + 0.125f, this.transform.position.z)
            , Quaternion.identity) as GameObject).GetComponent<CharacterStack>();
        characterStack.transform.rotation = Quaternion.Euler(0, direction, 0);
        characterStack.Initialize(pawnControllers);
        //hide previous pawns
        for (int i = 0; i < pawnControllers.Count - 1; i++) {
            pawnControllers[i].meshRenderer.enabled = false;
        }
        //change last pawn position
        pawnControllers[pawnControllers.Count - 1].transform.position = new Vector3(pawnControllers[pawnControllers.Count - 1].transform.position.x,
            1.25f, pawnControllers[pawnControllers.Count - 1].transform.position.z);
    }
}