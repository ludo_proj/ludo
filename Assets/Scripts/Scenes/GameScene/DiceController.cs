using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using TMPro;

public class DiceController : MonoBehaviour
{

    public Vector3 position = new Vector3(0f, 2f, 0f);
    public Quaternion rotation = Quaternion.identity;
    public CameraController cameraController;

    public List<List<Vector3>> steps = new List<List<Vector3>>{
        new List<Vector3> { new Vector3(-90, -1, -1) },
        new List<Vector3> { new Vector3(0, -1, 90), new Vector3(-180, -1, -90) },
        new List<Vector3> { new Vector3(0, -1, 180), new Vector3(180, -1, 0) },
        new List<Vector3> { new Vector3(0, -1, 0), new Vector3(180, -1, 180) },
        new List<Vector3> { new Vector3(0, -1, -90), new Vector3(-180, -1, 90) },
        new List<Vector3> { new Vector3(90, -1, -1) }
    };

    public Vector3 targetPosition = new Vector3(0f, 2f, 0f);

    public List<Vector3> listMove = new List<Vector3>();

    public SpriteRenderer shadow;
    public GameObject rollCountGO;

    private TextMeshProUGUI rollCountText;

    void Start() {
        rollCountText = rollCountGO.GetComponentInChildren<TextMeshProUGUI>();
        rollCountGO.transform.localScale = new Vector3(0f, 0f, 1f);
    }

    public void rollDice(int step, TweenCallback callback)
    {
        cameraController.setToDice();
        listMove.Clear();
        Vector3 tmpRotation;
        for (int i = 0; i < 4; i++)
        {
            tmpRotation = new Vector3(Random.Range(-720, 720), Random.Range(-720, 720), Random.Range(-720, 720));
            listMove.Add(tmpRotation);
        }
        tmpRotation = steps[step - 1][Random.Range(0, steps[step - 1].Count)];
        if (tmpRotation.y == -1 && tmpRotation.z == -1)
        {
            tmpRotation += new Vector3(0, Random.Range(-360, 360), Random.Range(-360, 360));
        }
        else
        {
            tmpRotation += new Vector3(0, Random.Range(-360, 360), 0);
        }
        listMove.Add(tmpRotation);

        for (int i = 0; i < 5; i++)
        {
            transform.DORotate(listMove[i], .4f, RotateMode.FastBeyond360).SetDelay(i * .4f);
        }
        transform.DOMove(targetPosition + new Vector3(0, Random.Range(8f, 15f), 0), .5f).SetEase(Ease.OutCirc).OnComplete(() =>
        {
            transform.DOMove(targetPosition, 1.5f).SetEase(Ease.OutBounce).OnComplete(()=> {
                if (callback != null) callback.Invoke();
                ShowCount(step);
            });
        });
        shadow.DOFade(0.1f, .6f).SetEase(Ease.OutCirc).OnComplete(() => {
            shadow.DOFade(.5f, 2f).SetEase(Ease.OutBounce);
        });
    }

    void ShowCount(int count) {
        rollCountText.text = count.ToString();
        rollCountGO.transform.DOScale(1f, .7f).SetEase(Ease.OutBack).OnComplete(HideCount);
    }

    void HideCount() {
        rollCountGO.transform.DOScale(0f, .5f).SetDelay(.8f).SetEase(Ease.OutBack);
    }

    void LateUpdate() {
        shadow.transform.rotation = Quaternion.Euler(90, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
    }
}