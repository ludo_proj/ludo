using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private Vector3 adjustPos = new Vector3(0, 4f, 0);

    public GameObject dice;

    private Vector3 diceOffset;
    private Vector3 basePosition;
    private Vector3 currentTargetPosition;
    private GameObject currentTarget;
    private Camera currentCamera;
    public bool selectPawn = false;
    public bool isMoveGesture = false;

    public float minX, maxX, minY, maxY, minZ, maxZ;

    void Start()
    {
        basePosition = transform.position;
        diceOffset = transform.position - dice.transform.position;
        currentTarget = dice;
        currentTargetPosition = currentTarget.transform.position;
        currentCamera = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        if (isMoveGesture) return;
        if (!selectPawn)
        {
            transform.position = Vector3.Lerp(transform.position, currentTarget.transform.position + diceOffset, Time.deltaTime * 5);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, currentTargetPosition + diceOffset, Time.deltaTime * 5);
        }
    }

    public void setPosition(GameObject target)
    {
        isMoveGesture = false;
        currentTarget = target;
    }

    public void setToDice()
    {
        isMoveGesture = false;
        currentTarget = dice;
    }

    public void setBounds(Bounds bounds)
    {
        isMoveGesture = false;
        float boundSize = bounds.extents.x + 12;
        selectPawn = true;
        currentTargetPosition = bounds.center + adjustPos;
        StartCoroutine(ResizeOrthoSize(5f, boundSize*1.2f));
    }

    public void resetCamera()
    {
        isMoveGesture = false;
        selectPawn = false;
        StartCoroutine(ResizeOrthoSize(currentCamera.orthographicSize, 5f));
    }

    IEnumerator ResizeOrthoSize(float from, float size)
    {
        float duration = .5f;
        float startTime = 0;
        while (startTime <= duration)
        {
            currentCamera.orthographicSize = Mathf.Lerp(from, size, startTime / duration);
            startTime += Time.deltaTime;
            yield return null;
        }
        yield return null;
    }

    public void updateGesturePosition(Vector3 deltaPosition)
    {
        isMoveGesture = true;
        Vector3 newPos = transform.position - deltaPosition;
        Vector3 clampedPosition = new Vector3(Mathf.Clamp(newPos.x, minX, maxX), Mathf.Clamp(newPos.y, maxY, minY), Mathf.Clamp(newPos.z, minZ, maxZ));
        transform.position = clampedPosition;
    }

    public void updateGestureScale(float deltaScale)
    {
        isMoveGesture = true;
        float result = Mathf.Clamp(currentCamera.orthographicSize + deltaScale, 4, 18);
        currentCamera.orthographicSize = Mathf.Lerp(currentCamera.orthographicSize, result, Time.timeScale * 5);
    }
}