﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerProfile : MonoBehaviour
{
    public Image profilePicture;
    public TextMeshProUGUI profileName;
    public string nickName;
    public int userId;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public void PlayerJoined(string name, int id)
    {
        nickName = name;
        userId = id;
        profileName.text = name;
        gameObject.SetActive(true);
        //profilePicture.gameObject.SetActive(true);
        //profileName.gameObject.SetActive(true);
    }

    public void ClearPlayerData()
    {
        nickName = "";
        userId = -1;
        profileName.text = "";
        gameObject.SetActive(false);
        //profilePicture.gameObject.SetActive(false);
        //profileName.gameObject.SetActive(false);
    }
}
