﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class StickerManager : MonoBehaviour {
    public struct StickerData {
        public int sourceIdx, targetIdx, winRank;
    }

    public enum StickerID {
        PlayerKick,
        PlayerWin,
        PlayerLose,
        PlayerTurn
    }

    public Dictionary<string, Sprite> stickerSprites;
    public bool isShowing = false;

    private StickerController currentSticker;

    // Use this for initialization
    void Start () {
        stickerSprites = new Dictionary<string, Sprite>();
        Sprite[] sprites = Resources.LoadAll<Sprite>("Stickers");
        foreach (Sprite spr in sprites) {
            stickerSprites.Add(spr.name, spr);
        }
    }

    public void ShowSticker(StickerID stickerID, StickerData data) {
        if (isShowing) return;
        string stickerPFName = "";
        switch (stickerID) {
            case StickerID.PlayerWin:
                stickerPFName = "Win Sticker";
                break;
            case StickerID.PlayerLose:
                stickerPFName = "Lose Sticker";
                break;
            case StickerID.PlayerTurn:
                stickerPFName = "Turn Sticker";
                break;
            default:
                stickerPFName = "Kick Sticker";
                break;
        }
        currentSticker = Instantiate(Resources.Load<GameObject>("Prefabs/Stickers/" + stickerPFName).GetComponent<StickerController>(),
            Base.Instance.upperUICanvas.transform);
        if (currentSticker != null) {
            isShowing = true;
            currentSticker.Initialize(data);
            if (currentSticker.autoHide)
                currentSticker.Play(HideSticker);
            else
                currentSticker.Play(null);
        }
    }

    public void HideSticker() {
        DOTween.To(() => currentSticker.transform.localScale, x => currentSticker.transform.localScale = x,
            new Vector3(1, 0, 1), .2f).SetEase(Ease.OutSine).OnComplete(() => {
                isShowing = false;
                Destroy(currentSticker.gameObject);
                currentSticker = null;
            });
    }
}
