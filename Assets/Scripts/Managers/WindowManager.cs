﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using System;
using DG.Tweening;
using Assets.Scripts.Base;

public class WindowManager : MonoBehaviour {

    static List<WindowBase> windowStack = new List<WindowBase>();
    static GameObject blockPanelGameObject;
    // Use this for initialization
    void Start () {

	}

    public static void ShowWindow(WindowBase windowBase,object data = null)
    {   
        windowBase.InitWithData(data);
        if (windowStack.Count == 0)
        {
            blockPanelGameObject = Instantiate(Resources.Load("Prefabs/WindowBlockPanel"), Base.Instance.upperUICanvas.transform) as GameObject;
            blockPanelGameObject.SetActive(true);
        }

        windowStack.Add(windowBase);
        Canvas canvas = windowBase.gameObject.GetComponent<Canvas>();
        canvas.sortingOrder = windowStack.Count + 1;
        blockPanelGameObject.GetComponent<Canvas>().sortingOrder = canvas.sortingOrder - 1;
        windowBase.gameObject.SetActive(true);

        windowBase.gameObject.transform.localScale = new Vector3(1, 0, 1);
        DOTween.To(() => windowBase.gameObject.transform.localScale,
            x => windowBase.gameObject.transform.localScale = x, new Vector3(1, 1, 1), .2f)
            .SetEase(Ease.InOutSine).OnComplete(() => {
                windowBase.OnShowed();
            });
    }
    
    public static void ShowWindow(string name,object data=null)
    {
        GameObject gameObject = (GameObject) Instantiate(Resources.Load("Prefabs/Windows/"+name), Base.Instance.upperUICanvas.transform);
        WindowBase windowBase = (WindowBase)gameObject.GetComponent<WindowBase>();
        if (windowBase == null) throw new Exception("must extends WindowBase");

        ShowWindow(windowBase,data);
    }

    public static void CloseWindow(WindowBase window)
    {
        DOTween.To(() => window.transform.localScale, x => window.transform.localScale = x,
            new Vector3(1, 0, 1), .2f).SetEase(Ease.OutSine).OnComplete(() =>
            {
                window.gameObject.SetActive(false);
                windowStack.Remove(window);
                window.OnClosed();
                blockPanelGameObject.GetComponent<Canvas>().sortingOrder = windowStack.Count;
                if (windowStack.Count == 0)
                {
                    blockPanelGameObject.SetActive(false);
                    Destroy(blockPanelGameObject);
                }
            });
    }

    public static void CloseCurrentActiveWindow()
    {
        if (windowStack.Count > 0)
        {
            WindowBase windowToClose = windowStack[windowStack.Count - 1];
            DOTween.To(() => windowToClose.transform.localScale,
                x => windowToClose.transform.localScale = x, new Vector3(1, 0, 1), .3f)
                .SetEase(Ease.OutSine)
                .OnComplete(() =>
                {
                    windowToClose.gameObject.SetActive(false);
                    windowStack.Remove(windowToClose);
                    windowToClose.OnClosed();
                    blockPanelGameObject.GetComponent<Canvas>().sortingOrder = windowStack.Count;
                    if (windowStack.Count == 0)
                    {
                        blockPanelGameObject.SetActive(false);
                        Destroy(blockPanelGameObject);
                    }
                });
        }
    }

	// Update is called once per frame
	void Update () {
	    
	}
}
