using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using ExitGames.Client.Photon;
using System;

public enum EnumPhoton
{
    JoinedRoom = 1,
    StartGame = 2,
    NextPlayerTurn = 3,
    RollDice = 4,
    Finish = 5,
    Pay = 6,
    ReceiveMoney = 7,
    PauseTime = 8,
    Continue = 9,
    GameOver = 10,
    Move = 11,
    Ready = 12,
    BeginPrivateGame = 171,
    StartWithBots = 173,
    SendChatMessage = 175,
    SendChatEmojiMessage = 176,
    AddFriend = 177,
}

public class NetworkManager : MonoBehaviour
{
    public const string DEFAULT_CUSTOM_ROOMNAME = "test";
    public const string VERSION = "v0.0.1";

    private bool playerIsRequired;

    public bool PlayerIsRequired { get { return PhotonNetwork.room.PlayerCount > 1; } }
    public string userID;
    public int id;
    public int PlayerIdx;
    public string roomName;
    public PlayerProfile[] playerList;

    /// <summary>
	/// currently ongoing turn number
	/// </summary>
    public static readonly string TurnPropKey = "Turn";

    /// <summary>
    /// start (server) time for currently ongoing turn (used to calculate end)
    /// </summary>
    public static readonly string TurnStartPropKey = "TStart";

    /// <summary>
    /// Finished Turn of Actor (followed by number)
    /// </summary>
    public static readonly string FinishedTurnPropKey = "FToA";

    public Action OnRoomJoined;
    public Action OnFailedToJoinRoom;

    void Start()
    {
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.ConnectUsingSettings(VERSION);
    }

    public bool joinRandomRoom()
    {
        if (PhotonNetwork.insideLobby) {
            PhotonNetwork.JoinRandomRoom(null, 0);
            return true;
        } else {
            if (OnFailedToJoinRoom != null) OnFailedToJoinRoom.Invoke();
            WindowManager.ShowWindow("WInfo", new WInfo.InfoData { title = "Connect Failed", desc = "Failed to connect. Please try again." });
            PhotonNetwork.JoinLobby();
        }
        return false;
    }

    public void joinRoom(string name)
    {
        PhotonNetwork.JoinRoom(name);
    }

    public void createRoom()
    {
        string name = DEFAULT_CUSTOM_ROOMNAME;
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, MaxPlayers = 4, PlayerTtl = 1000, EmptyRoomTtl = 1000 };
        PhotonNetwork.CreateRoom(name, roomOptions, TypedLobby.Default);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public int getPlayerIdx()
    {
        return PlayerIdx;
    }
    
    public void setPlayers(PlayerProfile[] value)
    {
        playerList = value;
        //setPlayerIdx();
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        PhotonNetwork.OnEventCall += onEventCall;
    }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive.
    /// </summary>
    void OnDisable()
    {
        PhotonNetwork.OnEventCall -= onEventCall;
    }

    void OnConnectedToMaster()
    {
        Base.Instance.textController.updateText("Connected to Master");
        // TypedLobby typedLobby = new TypedLobby("a", LobbyType.Default);
        // PhotonNetwork.JoinLobby(typedLobby);
    }

    void OnJoinedLobby()
    {
        Debug.Log("InsideLobby" + PhotonNetwork.insideLobby);
        Base.Instance.textController.updateText("Joined Lobby");
        // RoomOptions roomOptions = new RoomOptions() { isVisible = false, maxPlayers = 4 };
        // PhotonNetwork.JoinOrCreateRoom("test", roomOptions, TypedLobby.Default);
    }

    void OnJoinedRoom()
    {
        roomName = PhotonNetwork.room.Name;
        Base.Instance.textController.updateText("Joined Room: " + roomName);
        userID = PhotonNetwork.player.UserId;
        id = PhotonNetwork.player.ID;
        PhotonNetwork.RaiseEvent((int)EnumPhoton.JoinedRoom, 0, true, null);
        //for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        //{
        //    Base.Instance.textController.updateText(PhotonNetwork.playerList[i].ID + " - " + PhotonNetwork.playerList[i].UserId);
        //}
        if (PhotonNetwork.isMasterClient)
        {
            Hashtable hashtable = new Hashtable();
            // hashtable[userID] = EnumPhoton.JoinedRoom;
            hashtable["totalReady"] = 1;
            PhotonNetwork.room.SetCustomProperties(hashtable);
        }
        else
        {
            Base.Instance.textController.updateText("totalReady: " + PhotonNetwork.room.CustomProperties["totalReady"]);
        }
        if (OnRoomJoined != null) OnRoomJoined.Invoke();
    }

    void OnLeftRoom()
    {

    }

    void OnCreatedRoom()
    {
        Debug.Log("Successfuly create room with name: " + PhotonNetwork.room.Name);
    }

    void OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("Failed to create room");
        if (OnFailedToJoinRoom != null) OnFailedToJoinRoom.Invoke();
    }

    void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("Failed to join room");
        if (OnFailedToJoinRoom != null) OnFailedToJoinRoom.Invoke();
    }

    void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug.Log("Failed to join random room " + codeAndMsg[0].ToString());
        int code = int.Parse(codeAndMsg[0].ToString());
        //string message = codeAndMsg[1].ToString();
        if (code == 32760) // No Match Found
        {
            // Create Room
            createRoom();
        }
    }

    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        Debug.Log("Player joined: " + newPlayer.NickName + "(" + newPlayer.UserId + ")");
    }

    void onEventCall(byte eventcode, object content, int senderid)
    {
        // Debug.Log("eventCode: " + ((EnumPhoton)eventcode).ToString() + " content: " + content + " senderid: " + senderid);
        if (eventcode == (int)EnumPhoton.Ready)
        {
            if (PhotonNetwork.isMasterClient)
            {
                ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.room.CustomProperties;
                int totalPLayer = (int)hashtable["totalReady"];
                hashtable["totalReady"] = totalPLayer + 1;
                PhotonNetwork.room.SetCustomProperties(hashtable);
            }
        }
    }

    //temporary usage for room testing
    public string roomSelected = "";
    public bool JoinCertainRoom() {
        if (PhotonNetwork.insideLobby) {
            Base.Instance.textController.updateText("Join:" + roomSelected);
            if (!string.IsNullOrEmpty(roomSelected))
                JoinCustomRoom(roomSelected);
            else
                joinRandomRoom();
            return true;
        } else {
            if (OnFailedToJoinRoom != null) OnFailedToJoinRoom.Invoke();
            WindowManager.ShowWindow("WInfo", new WInfo.InfoData { title = "Connect Failed", desc = "Failed to connect. Please try again." });
            PhotonNetwork.JoinLobby();
        }
        return false;
    }

    public void JoinCustomRoom(string rName) {
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, MaxPlayers = 4, PlayerTtl = 1000, EmptyRoomTtl = 1000 };
        PhotonNetwork.JoinOrCreateRoom(rName, roomOptions, TypedLobby.Default);
    }

}